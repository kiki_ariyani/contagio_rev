package com.mobilus.contagio.activity;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.authentication.AccountGeneral;
import com.mobilus.contagio.helper.ShowCaseViewHelper;
import com.mobilus.contagio.model.Preference;
import com.mobilus.contagio.service.NotificationService;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.helper.GsonUtils;
import com.mobilus.contagio.model.User;
import com.mobilus.contagio.util.WindowCompatUtil;

import org.json.JSONObject;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static com.mobilus.contagio.authentication.AccountGeneral.sServerAuthenticate;

public class AuthenticationActivity extends AccountAuthenticatorActivity {
    private final String TAG = "AuthenticationActivity";

    /** AUTHENTICATION **/
    private AccountManager mAccountManager;
    private String mAuthTokenType;

    /** VIEW **/
    private AutoCompleteTextView loginEmail;
    private EditText loginPassword;
    private AutoCompleteTextView signupEmail;
    private EditText signupName;
    private EditText signupPassword;
    private EditText signupRetypePassword;
    private EditText signupPhone;
    private EditText fpEmail;
    private  View signupLayout;
    private View signinLayout;
    private View fpLayout;

    private ProgressDialog progressDialog;

    public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";

    public static final String KEY_ERROR_MESSAGE = "ERR_MSG";

    public final static String PARAM_USER_PASS = "USER_PASS";

    private CallbackManager callbackManager;
    private Profile profile;
    private Intent fbRes = new Intent();

    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Preference.getAsBoolean(Preference.KEY__IS_LOGIN_SKIPPED)){
            Intent intent = new Intent(getApplication(), NewsfeedActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_authentication);
        //WindowCompatUtil.initStatusBarColor(this, findViewById(R.id.main_layout));

        initElements();
        initFacebookLogin();

        // Hide the Action Bar
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        ActionBar actionBar = getActionBar();

        if (actionBar != null)
            actionBar.hide();

        mAccountManager = AccountManager.get(getBaseContext());

        String accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);
        mAuthTokenType = getIntent().getStringExtra(ARG_AUTH_TYPE);
        if (mAuthTokenType == null)
            mAuthTokenType = AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS;

        if (accountName != null) {
            loginEmail.setText(accountName);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("LOGIN_FB", "ActivityResult");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initElements() {
        // Content
        signupLayout = findViewById(R.id.signup_layout);
        signinLayout = findViewById(R.id.signin_layout);
        final TextView signinLink = (TextView) findViewById(R.id.signin_link);
        final TextView signupLink = (TextView) findViewById(R.id.signup_link);
        final TextView skipLink = (TextView) findViewById(R.id.skip_login_link);
        final TextView fpLink = (TextView) findViewById(R.id.forgot_password_link);
        final Button signupSubmit = (Button) findViewById(R.id.signup_submit);
        final Button signinSubmit = (Button) findViewById(R.id.signin_submit);
        loginEmail = (AutoCompleteTextView) findViewById(R.id.login_email);
        loginPassword = (EditText) findViewById(R.id.login_password);
        signupEmail = (AutoCompleteTextView) findViewById(R.id.signup_email);
        signupName = (EditText) findViewById(R.id.signup_name);
        signupPassword = (EditText) findViewById(R.id.signup_password);
        signupRetypePassword = (EditText) findViewById(R.id.signup_retype_password);
        signupPhone = (EditText) findViewById(R.id.signup_phone);
        fpLayout = findViewById(R.id.forgot_password_layout);
        final TextView fpSigninLink = (TextView) findViewById(R.id.fp_signin_link);
        final Button fpSubmit = (Button) findViewById(R.id.fp_submit);
        fpEmail = (EditText) findViewById(R.id.fp_email);

        signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                signinLayout.setVisibility(View.GONE);
                signupLayout.setVisibility(View.VISIBLE);
                fpLayout.setVisibility(View.GONE);
                signupEmail.requestFocus();
            }
        });

        signinLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupLayout.setVisibility(View.GONE);
                signinLayout.setVisibility(View.VISIBLE);
                fpLayout.setVisibility(View.GONE);
            }
        });

        fpLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signinLayout.setVisibility(View.GONE);
                signupLayout.setVisibility(View.GONE);
                fpLayout.setVisibility(View.VISIBLE);
            }
        });

        skipLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preference.set(Preference.KEY__IS_LOGIN_SKIPPED, true);

                Intent intent = new Intent(getApplication(), NewsfeedActivity.class);
                startActivity(intent);
                finish();
            }
        });

        fpSigninLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupLayout.setVisibility(View.GONE);
                signinLayout.setVisibility(View.VISIBLE);
                fpLayout.setVisibility(View.GONE);
            }
        });

        signupPhone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    submitSignup();
                    return true;
                }
                return false;
            }
        });

        loginPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    submitLogin();
                    return true;
                }
                return false;
            }
        });

        signinSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitLogin();
            }
        });

        signupSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitSignup();
            }
        });

        fpSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForgotPass();
            }
        });

        progressDialog = new ProgressDialog(AuthenticationActivity.this);

        prefs = getSharedPreferences("com.mobilus.contagio", MODE_PRIVATE);

        fillUpEmail();
        if(prefs.getBoolean("authfirstrun", true)){
            initShowCase();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("authfirstrun", true)) {
            prefs.edit().putBoolean("authfirstrun", false).commit();
        }
    }

    private void fillUpEmail() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(this).getAccounts();
        Set<String> emailSet = new HashSet<String>();

        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                emailSet.add(account.name);
            }
        }
        ArrayAdapter emailAutocomplete = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, new ArrayList<String>(emailSet));
        loginEmail.setAdapter(emailAutocomplete);
        signupEmail.setAdapter(emailAutocomplete);
    }

    private void submitSignup() {
        final String email = signupEmail.getText().toString();
        final String name = signupName.getText().toString();
        final String password = signupPassword.getText().toString();
        final String retype_password = signupRetypePassword.getText().toString();
        final String phone = signupPhone.getText().toString();
        final String account_type = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);

        // form validation for signup
        boolean formStatus = true;
        if( email.trim().equals("")) {
            formStatus = false;
            signupEmail.setError("Email is required");
        }
        if( name.trim().equals("")) {
            formStatus = false;
            signupName.setError("Name is required");
        }
        if( password.trim().equals("")) {
            formStatus = false;
            signupPassword.setError("Password is required");
        }
        if( !retype_password.trim().equals(password.trim())) {
            formStatus = false;
            signupRetypePassword.setError("Password do not match");
        }
        if( phone.trim().equals("")) {
            formStatus = false;
            signupPhone.setError("Phone is required");
        }

        if( formStatus) {
            WindowCompatUtil.hideSoftKeyboard(AuthenticationActivity.this);
            new SignupTask(this).execute(email, name, password, phone, account_type);
        }
    }

    public class SignupTask extends AsyncTask<String, Void, Intent> {

        public SignupTask(AccountAuthenticatorActivity activity) {
            this.activity = activity;
            progressDialog.setMessage(getString(R.string.signing_up));
        }

        private AccountAuthenticatorActivity activity; // application context

        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(final Intent intent) {
            if (intent.hasExtra(KEY_ERROR_MESSAGE)) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(getBaseContext(), intent.getStringExtra(KEY_ERROR_MESSAGE), Toast.LENGTH_LONG).show();
            } else {
                finishLogin(intent);
            }
        }

        protected Intent doInBackground(final String... args) {
            Log.i(TAG, "> Started signing up");

            final String email = args[0];
            final String name = args[1];
            final String password = args[2];
            final String phone = args[3];
            final String account_type = args[4];

            String authtoken = null;
            Bundle data = new Bundle();

            try {
                authtoken = sServerAuthenticate.userSignUp(email, name, password, phone);
                if (!authtoken.substring(0,1).equals("#")) { // success
                    data.putString(AccountManager.KEY_ACCOUNT_NAME, email);
                    data.putString(AccountManager.KEY_ACCOUNT_TYPE, account_type);
                    data.putString(AccountManager.KEY_AUTHTOKEN, authtoken);
                    data.putString(PARAM_USER_PASS, password);
                } else { // failed
                    data.putString(KEY_ERROR_MESSAGE, getString(R.string.error_signup) + " : " + authtoken.substring(1));
                }
            } catch (Exception e) {
                data.putString(KEY_ERROR_MESSAGE, getString(R.string.error_occurred) + " : " + e.getMessage());
            }

            final Intent res = new Intent();
            res.putExtras(data);
            return res;
        }
    }

    private void submitLogin() {
        WindowCompatUtil.hideSoftKeyboard(AuthenticationActivity.this);

        final String email = loginEmail.getText().toString();
        final String password = loginPassword.getText().toString();
        final String accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);

        // form validation for login
        boolean formStatus = true;
        if( email.trim().equals("")) {
            formStatus = false;
            loginEmail.setError("Email is required");
        }
        if( password.trim().equals("")) {
            formStatus = false;
            loginPassword.setError("Password is required");
        }

        if( formStatus) {
            new LoginTask(this).execute(email, password, accountType);
        }
    }

    private void finishLogin(Intent intent) {
        Log.d(TAG, "> finishLogin");

        // Start Notification Service (to detect nearby report/event when by user location)
        startService(new Intent(getBaseContext(), NotificationService.class));

        String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        String accountPassword = intent.getStringExtra(PARAM_USER_PASS);
        String accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);
        Log.i("finishLogin.accountType", "" + accountType);
//        final Account account = new Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));
        final Account account = new Account(accountName, accountType);

        if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
            Log.i(TAG, "> finishLogin > addAccountExplicitly");
            String authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            String authtokenType = mAuthTokenType;
            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager.addAccountExplicitly(account, accountPassword, null);
            mAccountManager.setAuthToken(account, authtokenType, authtoken);
        } else {
            Log.i(TAG, "> finishLogin > setPassword");
            mAccountManager.setPassword(account, accountPassword);
        }

        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        finish();
    }

    public class LoginTask extends AsyncTask<String, Void, Intent> {

        public LoginTask(AccountAuthenticatorActivity activity) {
            this.activity = activity;
            progressDialog.setMessage(getString(R.string.signing_in));
        }

        private ProgressDialog dialog;  // progress dialog to show user that the backup is processing.
        private AccountAuthenticatorActivity activity; // application context

        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(final Intent intent) {
            if (intent.hasExtra(KEY_ERROR_MESSAGE)) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(getBaseContext(), intent.getStringExtra(KEY_ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
            } else {
                finishLogin(intent);
            }
        }

        protected Intent doInBackground(final String... args) {
            Log.i(TAG, "> Started authenticating");

            final String email = args[0];
            final String password = args[1];
            final String accountType = args[2];

            SharedPreferences prefs = activity.getSharedPreferences(
                    getString(R.string.package_name), Context.MODE_PRIVATE);
            final String lastToken = prefs.getString(getString(R.string.package_name) + getString(R.string.preference_last_token), "");

            String authtoken = null;
            Bundle data = new Bundle();

            try {
                authtoken = sServerAuthenticate.userSignIn(email, password, mAuthTokenType, lastToken);
                Log.i(TAG, " > sign in authtoken: "+authtoken);
                if (!authtoken.substring(0,1).equals("#")) { // email & password are correct
                    data.putString(AccountManager.KEY_ACCOUNT_NAME, email);
                    data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                    data.putString(AccountManager.KEY_AUTHTOKEN, authtoken);
                    data.putString(PARAM_USER_PASS, password);
                } else { // email/password is incorrect
                    data.putString(KEY_ERROR_MESSAGE, getString(R.string.error_login) + " : " + authtoken.substring(1));
                }
            } catch (Exception e) {
                data.putString(KEY_ERROR_MESSAGE, getString(R.string.error_occurred) + " : " + e.getMessage());
            }

            final Intent res = new Intent();
            res.putExtras(data);
            return res;
        }
    }

    @Override
    public void onBackPressed() {
        Intent result = new Intent();
        Bundle b = new Bundle();
        result.putExtras(b);

        setAccountAuthenticatorResult(null); // null means the user cancelled the authorization processs
        setResult(RESULT_OK, result);
        finish();
    }

    private void initFacebookLogin(){
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_fb_button);
        loginButton.setLoginBehavior(LoginBehavior.WEB_ONLY);
        loginButton.setReadPermissions(Arrays.asList("email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                profile = Profile.getCurrentProfile();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (response.getError() != null) {
                            Toast.makeText(getApplicationContext(), response.getError().toString(), Toast.LENGTH_LONG).show();
                        } else {
                            progressDialog.setMessage("Please wait...");
                            progressDialog.show();

                            final String email = object.optString("email");
                            String name = object.optString("name");
                            String gender = object.optString("gender");
                            String fbId = object.optString("id");
                            Log.d("LOGIN_FB", "object : " + object);
                            Ion.with(AuthenticationActivity.this)
                                    .load(Appconfig.USERS_API_URL + "login_with_facebook/")
                                    .setBodyParameter("email", email)
                                    .setBodyParameter("name", name)
                                    .setBodyParameter("gender", gender)
                                    .setBodyParameter("fb_id", fbId)
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {

                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            if (e != null) {
                                                Log.d("LOGIN_FB", e.getMessage());
                                            } else {
                                                Log.d("LOGIN_FB", "result : " + result);
                                                if (result.get("status").getAsInt() == 1) {
                                                    JsonObject data = result.get("data").getAsJsonObject();
                                                    String authtoken = data.get("auth_token").getAsString();
                                                    Log.d(TAG, "json object string user: " + data);

                                                    User loggedUser = GsonUtils.getGson().fromJson(data.get("user").getAsJsonObject(), User.class);
                                                    Log.d(TAG, "loggedUser: " + loggedUser);
                                                    loggedUser.save();

                                                    final Bundle fbData = new Bundle();

                                                    fbData.putString(AccountManager.KEY_ACCOUNT_NAME, email);
                                                    fbData.putString(AccountManager.KEY_ACCOUNT_TYPE, getIntent().getStringExtra(ARG_ACCOUNT_TYPE));
                                                    fbData.putString(AccountManager.KEY_AUTHTOKEN, authtoken);
                                                    fbData.putString(PARAM_USER_PASS, "-1");

                                                    Log.d("LOGIN_FB", "Bundle : " + fbData.toString());
                                                    fbRes.putExtras(fbData);

                                                    if (fbRes.hasExtra(KEY_ERROR_MESSAGE)) {
                                                        if (progressDialog.isShowing()) {
                                                            progressDialog.dismiss();
                                                        }
                                                        Toast.makeText(getBaseContext(), "error : " + fbRes.getStringExtra(KEY_ERROR_MESSAGE), Toast.LENGTH_LONG).show();
                                                    } else {
                                                        finishLogin(fbRes);
                                                    }
                                                }

                                            }
                                        }
                                    });
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email, gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i("LOGIN_FB", "Cancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("LOGIN_FB", "Error");
            }
        });
    }

    private void initShowCase(){
        ViewTarget target = new ViewTarget(R.id.skip_login_link, this);
        ShowCaseViewHelper sv = new ShowCaseViewHelper();
        ViewTarget nextTarget = new ViewTarget(R.id.login_email, this);
        sv.show(AuthenticationActivity.this, target, getString(R.string.tutor_skip_login), getString(R.string.tutor_desc_skip_login), "CenterRight", nextTarget, getString(R.string.tutor_login), getString(R.string.tutor_desc_login), null, true);
    }

    private void submitForgotPass(){
        progressDialog.setMessage(getString(R.string.loading));

        String email = fpEmail.getText().toString();
        if(email.trim().equals("")){
            fpEmail.setError("Email is required");
        }else{
            progressDialog.show();
            Ion.with(this)
                    .load(Appconfig.USERS_API_URL + "forget_password")
                    .setBodyParameter("email", email)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, final JsonObject result) {
                            if (e != null) {
                                Log.i("FORGET_PASSWORD", e.getMessage());
                            } else {
                                if(progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                showAlert(result.get("status").getAsInt(),result.get("msg").getAsString(), null);
                            }
                        }
                    });
        }
    }

    private void showAlert(final int status,String message, View.OnClickListener onClickListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(message)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                if (status == 1) {
                                    fpEmail.setText("");
                                    signupLayout.setVisibility(View.GONE);
                                    signinLayout.setVisibility(View.VISIBLE);
                                    fpLayout.setVisibility(View.GONE);
                                }
                            }
                        }).create();

        alertDialog.show();
    }
}
