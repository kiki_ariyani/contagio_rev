package com.mobilus.contagio.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.adapter.ChooseImageAdapter;
import com.mobilus.contagio.adapter.PlacesAutoCompleteAdapter;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.helper.PlaceAPI;
import com.mobilus.contagio.model.Event;
import com.mobilus.contagio.model.EventImage;
import com.mobilus.contagio.model.Report;
import com.mobilus.contagio.model.ReportImage;
import com.mobilus.contagio.ui.ExpandableHeightGridView;
import com.mobilus.contagio.util.TextUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 29/10/2015.
 */
public class EventActivity extends BaseAppCompatActivity implements OnClickListener, OnItemClickListener {
    private Event event;
    private com.mobilus.contagio.model.Location eventLocation;
    private EditText name;
    private EditText datetime;
    private EditText time;
    private EditText organizer;
    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat dateTimeFormatter;
    private DatePickerDialog datePickerDialog;
    private AutoCompleteTextView venue;
    private int serverId = 0;
    private int serverLocId = 0;

    private View clickAreaDatetime;
    private View clickAreaTime;

    private Uri mImageCaptureUri;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    private ChooseImageAdapter eventImageAdapter;
    private ArrayList<JSONObject> eventImages = new ArrayList<>();
    final String STATUS_OK = "OK";

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for wifi status
    boolean isWifiEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60; // 1 minute
    private Location location; // location
    private String strVenue = "";

    private ProgressDialog dialog;
    private final String TAG = "EventActivity";

    private PlacesAutoCompleteAdapter placesAdapter;
    private Double locationLat;
    private Double locationLng;
    private String apiId;
    private int apiType;

    private View btnPhotoLayout;
    private ImageButton altImgBtn;
    private View altbtnPhotoLayout;
    private Bundle intentBundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            mImageCaptureUri = Uri.parse(savedInstanceState.getString("paramImageCaptureUri"));
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("New Event");
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);

        intentBundle = getIntent().getExtras();

        initElements();
        setDateTimeField();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("paramImageCaptureUri", String.valueOf(mImageCaptureUri));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.form_event;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveLocation();
            return true;
        }else if(id == android.R.id.home){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void initElements() {
        btnPhotoLayout = findViewById(R.id.btn_photo_layout);
        altbtnPhotoLayout = findViewById(R.id.alt_btn_photo_layout);
        altImgBtn = (ImageButton) findViewById(R.id.alt_upload_image);

        buildImageDialog();

        ExpandableHeightGridView gridView = (ExpandableHeightGridView) findViewById(R.id.gridviewimg);
        eventImageAdapter = new ChooseImageAdapter(this, eventImages, false, Appconfig.EVENT_IMAGE_ASSET_URL);
        eventImageAdapter.setOnItemEmpty(new ChooseImageAdapter.OnItemEmpty() {
            @Override
            public void onItemEmpty() {
                altbtnPhotoLayout.setVisibility(View.GONE);
                btnPhotoLayout.setVisibility(View.VISIBLE);

            }
        });
        gridView.setAdapter(eventImageAdapter);
        gridView.setExpanded(true);

        gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Intent i = new Intent(getApplicationContext(), ViewImagesActivity.class);
                i.putExtra("images", eventImages.toString());
                i.putExtra("position", position);
                i.putExtra("itemType", Appconfig.NEWSFEED_EVENT);
                startActivity(i);
            }
        });

        name = (EditText) findViewById(R.id.event_name);

        datetime = (EditText) findViewById(R.id.event_date);
        datetime.setInputType(InputType.TYPE_NULL);

        time = (EditText) findViewById(R.id.event_time);
        time.setInputType(InputType.TYPE_NULL);

        venue = (AutoCompleteTextView) findViewById(R.id.event_venue);

        Context context = this;
        getCoordinates(context);

        organizer = (EditText) findViewById(R.id.event_organizer);

        dialog = new ProgressDialog(EventActivity.this);

        clickAreaDatetime = findViewById(R.id.click_area_event_date);
        clickAreaTime = findViewById(R.id.click_area_event_time);

        if(intentBundle != null){
            if(intentBundle.containsKey("event")) {
                initEdit();
            }
        }
    }

    private void setDateTimeField() {
        clickAreaDatetime.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();

                view.updateDate(year, monthOfYear, dayOfMonth);
                newDate.set(year, monthOfYear, dayOfMonth);
                datetime.setText(dateFormatter.format(newDate.getTime()));

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        clickAreaTime.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(EventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time.setText(new StringBuilder().append(pad(selectedHour))
                                .append(":").append(pad(selectedMinute)));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.show();

            }
        });
    }

    @Override
    public void onClick(View v) {
        datePickerDialog.show();
    }

    private void buildImageDialog() {
        final String[] items = new String[]{"From Camera", "From Gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment
                            .getExternalStorageDirectory(), "tmp_img_"
                            + String.valueOf(System.currentTimeMillis())
                            + ".jpg");
                    mImageCaptureUri = Uri.fromFile(file);

                    try {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                        intent.putExtra("return-data", true);
                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dialog.cancel();
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");

                    startActivityForResult(intent, PICK_FROM_FILE);
                }
            }
        });

        final AlertDialog dialog = builder.create();

        btnPhotoLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }

        });

        altImgBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        } else {
            String path = "";

            if (requestCode == PICK_FROM_FILE) {
                mImageCaptureUri = data.getData();
                path = getRealPathFromURI(mImageCaptureUri); // from Gallery

                if (path == null)
                    path = mImageCaptureUri.getPath(); // from File Manager
            } else {
                if (mImageCaptureUri != null)
                    path = mImageCaptureUri.getPath(); // Get image from camera
            }

            // Declare new problem image
            if (path != "") {
                JSONObject img = new JSONObject();

                try {
                    img.put("id", "-1");
                    img.put("name", TextUtil.generateRandomString());
                    img.put("path", path);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                eventImages.add(img);
            }
            eventImageAdapter.notifyDataSetChanged();

            btnPhotoLayout.setVisibility(View.GONE);
            altbtnPhotoLayout.setVisibility(View.VISIBLE);
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);

        if (cursor == null)
            return null;

        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public Location getCoordinates(Context context) {
        if ( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        try {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            // getting wifi status
            isWifiEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                Toast.makeText(getApplicationContext(),
                        "Issue Location not found. No location provider is enabled", Toast.LENGTH_LONG)
                        .show();
            } else {
                this.canGetLocation = true;

                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                    Log.i("mylocation", "LOC Network Enabled");

                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if (location != null) {
                        Log.i("mylocation", "LOC by Network");
                        //  fillUpAddress(address, location);
                    }

                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES,locationListener);
                        Log.d("activity", "RLOC: GPS Enabled");
                        Log.i("mylocation", "RLOC: loc by GPS");
                        //   fillUpAddress(address, location);
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            Log.i("mylocation", "RLOC: loc by GPS");
                            //  fillUpAddress(address, location);
                        }
                    }
                }

                placesAdapter = new PlacesAutoCompleteAdapter(this, R.layout.item_place, location);
                venue.setAdapter(placesAdapter);
                venue.setOnItemClickListener(EventActivity.this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    // Define a listener that responds to location updates
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location mLocation) {
            //   location = mLocation;
            //   fillUpAddress(address, location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private void fillUpAddress(final EditText editText, Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        editText.setHint(R.string.loading_address);
        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                + latitude + "," + longitude;
        Ion.with(this).load(url).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e != null) {
                    editText.setText(getString(R.string.unknown_location));
                    return;
                }
                String status = result.get("status").getAsString();
                if (status.equalsIgnoreCase(STATUS_OK)) {
                    JsonArray locationArray = result.get("results").getAsJsonArray();
                    JsonObject locationObject = locationArray.get(0).getAsJsonObject();
                    strVenue = locationObject.get("formatted_address").getAsString();
                    if (venue.length() > 0) {
                        editText.setText(strVenue);
                    }else{
                        editText.setHint(R.string.venue);
                    }
                }
            }
        });
    }

    private boolean save() {
        final boolean[] retval = {true};

        if (!name.getText().toString().trim().isEmpty() && !venue.getText().toString().trim().isEmpty()) {
            String strDate = datetime.getText().toString();
            String strTime = time.getText().toString();
            Date dDate = null;
            try {
                dDate = dateTimeFormatter.parse(strDate+" "+strTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long dateInMili = dDate.getTime();
            long time= System.currentTimeMillis();

            JsonObject json = new JsonObject();
            json.addProperty("id", serverId);
            json.addProperty("user_id", String.valueOf(Credential.getUser().getIdServer()));
            json.addProperty("location_id", eventLocation.getServerId());
            json.addProperty("name", name.getText().toString().trim());
            json.addProperty("datetime", dateInMili);
            json.addProperty("organizer", organizer.getText().toString().trim());
            json.addProperty("created_at", time);
            json.addProperty("updated_at", time);
            json.addProperty("deleted_images", String.valueOf(eventImageAdapter.getDeletedImg()));
            Log.i("SAVE_EVENT : ",json+"");
            Ion.with(this)
                    .load(Appconfig.EVENTS_API_URL + "save")
                    .setHeader(Credential.AUTH, Credential.AUTH_TYPE + Credential.getAuthToken())
                    .setJsonObjectBody(json)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            Log.i("SAVE_EVENT_RESULT : ",result+"");
                            if (e != null) {
                                Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                            } else {
                                if (result.get("status").getAsInt() == 1) {
                                    event = new Event(result.get("data").getAsJsonObject(), eventLocation);
                                    event.save();

                                    Log.i(TAG, "event images count : " + eventImages.size());

                                    if (eventImages.size() > 0) {
                                        retval[0] = saveImages(); //upload problem images
                                    }else{
                                        dialog.dismiss();
                                        showAlert(result.get("msg").getAsString(), null);
                                    }
                                }else {
                                    showAlert(result.get("msg").getAsString(), null);
                                }
                            }
                        }
                    });

        }else{
            if(name.getText().toString().trim().equals("")){
                name.setError("Event Name is required");
            }
            if(venue.getText().toString().trim().equals("")){
                venue.setError("Venue is required");
            }
        }
        return retval[0];
    }

    private boolean saveImages() {
        final boolean[] status = {true};
        long time= System.currentTimeMillis();

        event.deleteImages();

        for (JSONObject jsonEventImage : eventImages) {
            EventImage eventImage = new EventImage();
            eventImage.setEvent(event);
            eventImage.setServerId(jsonEventImage.optString("id"));
            eventImage.setName(jsonEventImage.optString("name"));
            eventImage.setPath(jsonEventImage.optString("path"));
            eventImage.setCreatedAt(time);
            eventImage.setUpdatedAt(time);

            eventImage.save();

            if (eventImage.getServerId() == "-1") {
                Ion.with(this)
                        .load(Appconfig.EVENTS_API_URL + "upload_image")
                        .setHeader(Credential.AUTH, Credential.AUTH_TYPE + Credential.getAuthToken())
                        .setMultipartParameter("id", eventImage.getId().toString())
                        .setMultipartParameter("event_id", event.getServerId().toString())
                        .setMultipartParameter("name", eventImage.getName().toString())
                        .setMultipartParameter("path",eventImage.getPath().toString())
                        .setMultipartParameter("created_at", String.valueOf(eventImage.getCreatedAt()))
                        .setMultipartParameter("updated_at", String.valueOf(eventImage.getUpdatedAt()))
                        .setMultipartFile("file", new File(eventImage.getPath()))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, final JsonObject result) {
                                if (e != null) {
                                    Log.i("SAVE_IMG_E", e.getMessage());
                                } else {
                                    if (result.get("status").getAsInt() == 1) {
                                        Log.i(TAG, "upload image result : " + result.get("msg"));
                                        status[0] = status[0] && true;
                                    } else {
                                        status[0] = status[0] && false;
                                    }

                                    if (dialog.isShowing()) {
                                        dialog.dismiss();
                                        showAlert(result.get("msg").getAsString(), null);
                                    }
                                }
                            }
                        });
            }
        }
        return status[0];
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String str = placesAdapter.getResultData(position);
        int resultSource = placesAdapter.getResultSource();
        try {
            JSONObject locationData = new JSONObject(str);

            if(resultSource == Appconfig.SOURCE_FOURSQUARE){
                locationLat = locationData.getJSONObject("location").getDouble("lat");
                locationLng = locationData.getJSONObject("location").getDouble("lng");
                apiId = locationData.getString("id");
            }else{
                PlaceAPI mPlaceApi = new PlaceAPI();
                Location latLng = mPlaceApi.placesDetail(locationData.getString("place_id"));
                if(latLng != null){
                    locationLat = latLng.getLatitude();
                    locationLng = latLng.getLongitude();
                    apiId = locationData.getString("place_id");
                }
            }

            apiType = resultSource;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean saveLocation() {
        final boolean[] status = {true};
        long time = System.currentTimeMillis();

        if (name.getText().toString().trim().isEmpty() || venue.getText().toString().trim().isEmpty()) {
            if(name.getText().toString().trim().equals("")){
                name.setError("Event Name is required");
            }
            if(venue.getText().toString().trim().equals("")){
                venue.setError("Venue is required");
            }
        }else{
            if(locationLat != null && locationLng != null){
                dialog.setMessage(getString(R.string.loading));
                dialog.show();

                JsonObject json = new JsonObject();
                json.addProperty("id", 0);
                json.addProperty("name", venue.getText().toString().trim());
                json.addProperty("lat", locationLat);
                json.addProperty("lng", locationLng);
                json.addProperty("api_id", apiId);
                json.addProperty("api_type", apiType);
                json.addProperty("created_at", time);
                Log.i("SAVE_LOCATION", json + "");
                Ion.with(this)
                        .load(Appconfig.LOCATIONS_API_URL + "save")
                        .setHeader(Credential.AUTH, Credential.AUTH_TYPE + Credential.getAuthToken())
                        .setJsonObjectBody(json)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e != null) {
                                    Log.i("SAVE_LOCATION_E", e.getMessage());
                                } else {
                                    if (result.get("status").getAsInt() == 1) {
                                        eventLocation = new com.mobilus.contagio.model.Location(result.get("data").getAsJsonObject());
                                        eventLocation.save();

                                        //save event
                                        status[0] = save();
                                    } else {
                                        dialog.dismiss();
                                        Log.i("SAVE_LOCATION_E", result.get("msg").getAsString());
                                    }
                                }
                            }
                        });
            }else{
                venue.setError("Venue not found.");
            }
        }

        return status[0];
    }

    private void showAlert(String message, OnClickListener onClickListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(message)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                EventActivity.this.finish();
                            }
                        }).create();

        alertDialog.show();
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private void initEdit(){
        String eventStr = getIntent().getStringExtra("event");
        event = new Gson().fromJson(eventStr, Event.class);

        List<EventImage> listImages = event.getImages();

        for(int i=0; i < listImages.size();i++){
            JSONObject itemImg = new JSONObject();
            try {
                itemImg.put("id", listImages.get(i).getServerId());
                itemImg.put("name", listImages.get(i).getName());
                itemImg.put("path", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            eventImages.add(itemImg);
        }
        eventImageAdapter.notifyDataSetChanged();

        if(eventImages.size() > 0){
            altbtnPhotoLayout.setVisibility(View.VISIBLE);
            btnPhotoLayout.setVisibility(View.GONE);
        }

        name.setText(event.getName());
        datetime.setText(DateFormat.format("yyyy-MM-dd", new Date(event.getDateTime())));
        time.setText(DateFormat.format("H:m", new Date(event.getDateTime())));
        organizer.setText(event.getOrganizer());
        venue.setText(event.getLocation().getName());

        locationLat = Double.valueOf(event.getLocation().getLat());
        locationLng = Double.valueOf(event.getLocation().getLng());
        apiId = event.getLocation().getApiId();
        serverId = Integer.parseInt(event.getServerId());
        serverLocId = Integer.parseInt(event.getLocation().getServerId());
    }
}
