package com.mobilus.contagio.activity;

import android.os.Bundle;

import com.mobilus.contagio.R;

/**
 * Created by user on 02/12/2015.
 */
public class ContactUsActivity extends DrawerActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarTitle("Contact Us");
    }

    @Override
    protected int getContentView() {
        return R.layout.contact_us;
    }
}
