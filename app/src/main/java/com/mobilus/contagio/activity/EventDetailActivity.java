package com.mobilus.contagio.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.helper.ContentShare;
import com.mobilus.contagio.helper.ShowCaseViewHelper;
import com.mobilus.contagio.helper.ToolbarActionItemTarget;
import com.mobilus.contagio.model.Event;
import com.mobilus.contagio.model.EventImage;
import com.mobilus.contagio.util.NetworkUtil;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 05/11/2015.
 */
public class EventDetailActivity extends BaseAppCompatActivity{
    private TextView eventName;
    private TextView eventDate;
    private TextView eventVenue;
    private TextView eventOrganizer;
    private Event event;
    private ViewPager mImagePager;
    private CirclePageIndicator mIndicator;
    private FrameLayout mImagesWrap;
    private List<EventImage> images;
    private Context context;

    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String strEvent = getIntent().getStringExtra("event");
        event = new Gson().fromJson(strEvent, Event.class);
        String actionBarTitle = event.getName();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(actionBarTitle);

       initElements();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.event_detail;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.content_detail, menu);

        MenuItem item = menu.findItem(R.id.action_edit);
        int creatorId = Integer.valueOf(event.getUserId());
        if(Credential.getUser() != null){
            if(creatorId != Credential.getUser().getIdServer()) {
                item.setVisible(false);
            }
        }else{
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            ContentShare intentShare = new ContentShare();
            intentShare.share(EventDetailActivity.this, "Contageo Shared Event", "Event : "+event.getName()+"\nVenue : "+event.getLocation().getName()+"\nDate : "+DateFormat.format("MMMM dd, yyyy, k:m a", new Date(event.getDateTime()))+"\nOrganizer : "+event.getOrganizer());
            return true;
        }else if(id == R.id.action_edit){
            final Intent i = new Intent(context, EventActivity.class);
            i.putExtra("event", new Gson().toJson(event));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //  context.startActivity(i);
                    startActivityForResult(i, 1);
                }
            }, 300);
        }else if(id == android.R.id.home){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void initElements(){
        context = this;
        eventName = (TextView) findViewById(R.id.event_name);
        eventName.setText(event.getName());

        eventDate = (TextView) findViewById(R.id.event_date);
        eventDate.setText(DateFormat.format("MMMM dd, yyyy, k:m a", new Date(event.getDateTime())));

        eventVenue = (TextView) findViewById(R.id.event_venue);
        eventVenue.setText(event.getLocation().getName());

        eventOrganizer = (TextView) findViewById(R.id.event_organizer);
        eventOrganizer.setText(event.getOrganizer());

        mImagePager = (ViewPager) findViewById(R.id.image_pager);
        mIndicator = (CirclePageIndicator) findViewById(R.id.img_indicator);
        mImagesWrap = (FrameLayout) findViewById(R.id.images_wrap);

        prefs = getSharedPreferences("com.mobilus.contagio", MODE_PRIVATE);

        if(prefs.getBoolean("sharefirstrun", true)) {
            ShowCaseViewHelper sv = new ShowCaseViewHelper();
            ToolbarActionItemTarget target = new ToolbarActionItemTarget(getToolbar(), R.id.action_share);
            sv.showToolbarAsTarget(EventDetailActivity.this, target, getString(R.string.tutor_share_event), getString(R.string.tutor_desc_share_event), false, null);
        }

        images = event.getImages();
        if(images.size() > 0) {
            mImagesWrap.setVisibility(View.VISIBLE);
            mImagePager.setAdapter(new EventImagePagerAdapter(images));
            mIndicator.setViewPager(mImagePager);
        }else{
            populateImages();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("sharefirstrun", true)) {
            prefs.edit().putBoolean("sharefirstrun", false).commit();
        }
    }

    private void populateImages() {
        if (!NetworkUtil.isConnected(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "No connection", Toast.LENGTH_LONG).show();
            return;
        }

        FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (result.get("status").getAsInt() > 0) {
                    Log.d("EventDetailActivity", "report images: " + result);
                    mImagesWrap.setVisibility(View.VISIBLE);
                    event.deleteImages();
                    images.clear();
                    JsonArray resultArray = result.get("data").getAsJsonArray();

                    EventImage image;
                    for (JsonElement jo : resultArray) {
                        image = new EventImage(jo.getAsJsonObject(), event);
                        image.save();
                        images.add(image);
                    }

                    mImagePager.setAdapter(new EventImagePagerAdapter(images));
                    mIndicator.setViewPager(mImagePager);
                }
            }
        };
        EventImage.getList(this, event.getServerId(), callback);
    }

    private class EventImagePagerAdapter extends PagerAdapter {
        private List<EventImage> mImageList;
        private ArrayList<JSONObject> imgs = new ArrayList<>();

        public EventImagePagerAdapter(List<EventImage> images) {
            mImageList = images;

            for (EventImage image : mImageList) {
                JSONObject img = new JSONObject();
                try {
                    img.put("id", image.getServerId());
                    img.put("name", image.getName());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                imgs.add(img);
            }
        }

        @Override
        public int getCount() {
            return mImageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            ImageView imgView = new ImageView(container.getContext());
            final EventImage item = mImageList.get(position);

            Ion.with(imgView).load(Appconfig.EVENT_IMAGE_ASSET_URL + item.getName());
            imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), ViewImagesActivity.class);
                    i.putExtra("images", imgs.toString());
                    i.putExtra("position", position);
                    i.putExtra("itemType", Appconfig.NEWSFEED_EVENT);
                    v.getContext().startActivity(i);
                }
            });

            container.addView(imgView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return imgView;
        }
    }
}
