package com.mobilus.contagio.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.mobilus.contagio.R;
import com.mobilus.contagio.adapter.NewsfeedImagePagerAdapter;
import com.mobilus.contagio.adapter.ReportAdapter;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.helper.ContentShare;
import com.mobilus.contagio.helper.ShowCaseViewHelper;
import com.mobilus.contagio.helper.ToolbarActionItemTarget;
import com.mobilus.contagio.model.Event;
import com.mobilus.contagio.model.EventImage;
import com.mobilus.contagio.model.Location;
import com.mobilus.contagio.model.Newsfeed;
import com.mobilus.contagio.model.NewsfeedImage;
import com.mobilus.contagio.model.Report;
import com.mobilus.contagio.model.ReportImage;
import com.mobilus.contagio.util.NetworkUtil;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.Date;
import java.util.List;

public class NewsfeedDetailActivity extends BaseAppCompatActivity {
    private static final String TAG = "NewsfeedDetailActivity";
    private Newsfeed newsfeed;
    private List<NewsfeedImage> images;
    private ReportAdapter mReportAdapter;
    private Location location;
    private List<Report> reportList;

    private ViewPager mImagePager;
    private CirclePageIndicator mIndicator;
    private FrameLayout mImagesWrap;
    private TextView updateErr;
    private RecyclerView mLatestUpdateList;
    private LinearLayoutManager mLayoutManager;
    private int caseReportNum = 0;
    private int breadingReportNum = 0;
    private int foggingReportNum = 0;
    private int bittenReportNum = 0;
    private int itemType;
    private Context context;
    private ProgressDialog progressDialog;

    private String tutorTitle;
    private String tutorDesc;

    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        progressDialog = new ProgressDialog(context);
        String newsfeedJson = getIntent().getStringExtra("newsfeed");
        newsfeed = new Gson().fromJson(newsfeedJson, Newsfeed.class);

        prefs = getSharedPreferences("com.mobilus.contagio", MODE_PRIVATE);

        if(newsfeed != null){
            fillView();
        }else{
            Toast.makeText(getApplicationContext(), "Newsfeed Detail not found", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_newsfeed_detail;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.content_detail, menu);
        MenuItem item = menu.findItem(R.id.action_edit);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            String subject = "";
            String content = "";
           if(newsfeed.getType() == Appconfig.NEWSFEED_LOCATION){
                subject = "Contageo Shared Location";
                content = "Contageo App report on\nLocation : "+newsfeed.getAddress()+"\nCase Report : "+caseReportNum+"\nBreeding Report : "+breadingReportNum+"\nFogging Report : "+foggingReportNum+"\nBitten Report :"+bittenReportNum;
            }
            ContentShare intentShare = new ContentShare();
            intentShare.share(NewsfeedDetailActivity.this, subject, content);
            return true;
        }else if(id == android.R.id.home){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            Log.d("ReportDetail","activity result");
            if(resultCode == RESULT_OK){
                String reportStr = data.getStringExtra("report");

                Report report = new Gson().fromJson(reportStr, Report.class);
                JsonObject newsfeedObj = new JsonObject();
                newsfeedObj.addProperty("id",report.getServerId());
                newsfeedObj.addProperty("user_id",report.getUserId());
                newsfeedObj.addProperty("report_type",report.getType());
                newsfeedObj.addProperty("address",report.getLocation().getName());
                newsfeedObj.addProperty("lat",report.getLocation().getLat());
                newsfeedObj.addProperty("lng",report.getLocation().getLng());
                newsfeedObj.addProperty("comment",report.getComment());
                newsfeedObj.addProperty("date",report.getDate());
                newsfeedObj.addProperty("created_at",report.getCreatedAt());
                newsfeedObj.addProperty("updated_at",report.getUpdatedAt());
                newsfeedObj.addProperty("event_name","");
                newsfeedObj.addProperty("creator","");
                newsfeedObj.addProperty("datetime",0);
                newsfeedObj.addProperty("organizer","");
                newsfeedObj.addProperty("case_num_week",0);
                newsfeedObj.addProperty("victims",report.getVictim());
                newsfeedObj.addProperty("last_case_date",0);
                newsfeedObj.addProperty("distance",0);
                newsfeedObj.addProperty("type",2);

                newsfeed = new Newsfeed(newsfeedObj);
                fillView();
            }
        }
    }

    private void fillView(){
        this.itemType = newsfeed.getType();
        if(newsfeed.getType() == Appconfig.NEWSFEED_LOCATION){
            setTitle(newsfeed.getAddress());

            int resourceIcon = R.drawable.pin;

            getSupportActionBar().setLogo(resourceIcon);
        }

        View locationContent = findViewById(R.id.location_content);
        mImagePager = (ViewPager) findViewById(R.id.image_pager);
        mIndicator = (CirclePageIndicator) findViewById(R.id.img_indicator);
        mImagesWrap = (FrameLayout) findViewById(R.id.images_wrap);
        updateErr = (TextView) findViewById(R.id.update_error);
        mLayoutManager = new LinearLayoutManager(this);
        mLatestUpdateList = (RecyclerView) findViewById(R.id.latest_update_list);


        tutorTitle = getString(R.string.tutor_share_location);
        tutorDesc = getString(R.string.tutor_desc_share_location);
        locationContent.setVisibility(View.VISIBLE);
        getLocationDetail();

        if(prefs.getBoolean("sharefirstrun", true)) {
            ShowCaseViewHelper sv = new ShowCaseViewHelper();
            ToolbarActionItemTarget target = new ToolbarActionItemTarget(getToolbar(), R.id.action_share);
            sv.showToolbarAsTarget(NewsfeedDetailActivity.this, target, tutorTitle, tutorDesc, false, null);
        }

        images = newsfeed.getImages();
        if(images.size() > 0) {
            mImagesWrap.setVisibility(View.VISIBLE);
            mImagePager.setAdapter(new NewsfeedImagePagerAdapter(images, newsfeed));
            mIndicator.setViewPager(mImagePager);
        }else {
            populateImagesList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("sharefirstrun", true)) {
            prefs.edit().putBoolean("sharefirstrun", false).commit();
        }
    }

    private void populateImagesList() {
        if (!NetworkUtil.isConnected(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "No connection", Toast.LENGTH_LONG).show();
            return;
        }
        FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (result.get("status").getAsInt() > 0) {
                    mImagesWrap.setVisibility(View.VISIBLE);
                    newsfeed.deleteImages();
                    images.clear();
                    JsonArray resultArray = result.get("data").getAsJsonArray();

                    NewsfeedImage newsfeedImage;
                    for (JsonElement jo : resultArray) {
                        newsfeedImage = new NewsfeedImage(jo.getAsJsonObject(), newsfeed);
                        newsfeedImage.save();
                        images.add(newsfeedImage);
                    }

                    mImagePager.setAdapter(new NewsfeedImagePagerAdapter(images, newsfeed));
                    mIndicator.setViewPager(mImagePager);
                }
            }
        };
        NewsfeedImage.getList(this, newsfeed.getServerId(), newsfeed.getType(), callback);
    }

    private void getLocationDetail(){
        final List<Location> locations = Location.find(Location.class, "server_id = ? ", newsfeed.getServerId() + "");
        reportList = Report.find(Report.class, "location = ? ", location + "");
        mLatestUpdateList.setLayoutManager(mLayoutManager);

        if(locations.size() > 0){
            location = locations.get(0);
            mReportAdapter = new ReportAdapter(reportList, location);
            mLatestUpdateList.setAdapter(mReportAdapter);
        }

        if (NetworkUtil.isConnected(getApplicationContext())) {
            FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (result.get("status").getAsInt() > 0) {
                        JsonObject data = result.get("data").getAsJsonObject();

                        location = new Location(data);
                        location.save();
                        mReportAdapter = new ReportAdapter(reportList, location);
                        mLatestUpdateList.setAdapter(mReportAdapter);
                        populateLocationReport();
                    }else{
                        updateErr.setText(result.get("msg").getAsString());
                    }
                }
            };
            Location.getOne(this, newsfeed.getServerId(), callback);
        }
    }

    private void populateLocationReport(){
        if (!NetworkUtil.isConnected(getApplicationContext())) {
            updateErr.setText("No connection");
        }

        FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (result.get("status").getAsInt() > 0) {
                    JsonArray data = result.get("data").getAsJsonArray();

                    Report.deleteAll(Report.class, "location = ?", location + "");
                    for (JsonElement jo : data) {
                        JsonObject lastUpdate = jo.getAsJsonObject();
                        int lastUpdateType = lastUpdate.get("type").getAsInt();
                        if(lastUpdateType == Appconfig.CASE_REPORT){
                            caseReportNum++;
                        }else if(lastUpdateType == Appconfig.BREADING_REPORT){
                            breadingReportNum++;
                        }else if(lastUpdateType == Appconfig.FOGGING_REPORT){
                            foggingReportNum++;
                        }/*else if(lastUpdateType == Appconfig.BITTEN_REPORT){
                            bittenReportNum++;
                        }*/

                        Report report = new Report(jo.getAsJsonObject(), location);
                        report.save();
                        reportList.add(report);
                    }
                    mReportAdapter.notifyDataSetChanged();
                }else{
                    updateErr.setText(result.get("msg").getAsString());
                }
            }
        };
        Newsfeed.getLocationReport(this, newsfeed.getServerId(), callback);
    }
}
