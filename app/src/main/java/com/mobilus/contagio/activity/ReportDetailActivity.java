package com.mobilus.contagio.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.helper.ContentShare;
import com.mobilus.contagio.helper.ShowCaseViewHelper;
import com.mobilus.contagio.helper.ToolbarActionItemTarget;
import com.mobilus.contagio.model.Report;
import com.mobilus.contagio.model.ReportImage;
import com.mobilus.contagio.util.NetworkUtil;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportDetailActivity extends BaseAppCompatActivity {
    private static final String TAG = "NewsfeedDetailActivity";
    private Report report;
    private List<ReportImage> images;
    private ViewPager mImagePager;
    private CirclePageIndicator mIndicator;
    private FrameLayout mImagesWrap;
    private Context context;

    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String reportStr = getIntent().getStringExtra("report");
        report = new Gson().fromJson(reportStr, Report.class);

        fillView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_report_detail;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.content_detail, menu);
        MenuItem item = menu.findItem(R.id.action_edit);
        int creatorId = Integer.valueOf(report.getUserId());
        if(Credential.getUser() != null) {
            if (creatorId != Credential.getUser().getIdServer()) {
                item.setVisible(false);
            }
        }else{
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            ContentShare intentShare = new ContentShare();
            intentShare.share(ReportDetailActivity.this, "Contageo Shared Report", "Contageo App report on \nLocation : "+report.getLocation().getName()+"\nReport Type : "+Appconfig.REPORT_TYPES[report.getType()]+"\nComment : " + report.getComment() + "\nDate :  " + DateFormat.format("MMMM dd, yyyy", new Date(report.getDate()))+"\nReported at : "+DateFormat.format("MMMM dd, yyyy, k:m a", new Date(report.getCreatedAt()))+"\nNumber of Victim : "+report.getVictim());
            return true;
        }else if(id == R.id.action_edit){
            final Intent i = new Intent(context, ReportActivity.class);
            i.putExtra("report", new Gson().toJson(report));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                  //  context.startActivity(i);
                    startActivityForResult(i, 1);
                }
            }, 300);
        }else if(id == android.R.id.home){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                    String reportStr = data.getStringExtra("report");
                    report = new Gson().fromJson(reportStr, Report.class);
                    fillView();
            }
        }
    }

    private void fillView(){
        setTitle(report.getLocation().getName());

        int resourceIcon = R.drawable.breeding;

        switch (report.getType()){
            case Appconfig.FOGGING_REPORT :
                resourceIcon = R.drawable.fogging;
                break;
          /*  case Appconfig.BITTEN_REPORT :
                resourceIcon = R.drawable.bitten;
                break;*/
            case Appconfig.CASE_REPORT :
                resourceIcon = R.drawable.report;
                break;
        }

        getSupportActionBar().setLogo(resourceIcon);
        context = this;

        TextView reportCreatedDate = (TextView) findViewById(R.id.report_created_date);
        TextView reportDate = (TextView) findViewById(R.id.report_date);
        TextView reportLocation = (TextView) findViewById(R.id.report_location);
        TextView reportComment = (TextView) findViewById(R.id.report_comment);
        View reportVictimWrapper = findViewById(R.id.report_victim_wrapper);
        TextView reportVictim = (TextView) findViewById(R.id.report_victim);
        mImagePager = (ViewPager) findViewById(R.id.image_pager);
        mIndicator = (CirclePageIndicator) findViewById(R.id.img_indicator);
        mImagesWrap = (FrameLayout) findViewById(R.id.images_wrap);

        reportCreatedDate.setText(DateFormat.format("MMMM dd, yyyy, k:m a", new Date(report.getCreatedAt())).toString());
        reportDate.setText(DateFormat.format("MMMM dd, yyyy", new Date(report.getDate())).toString());
        reportLocation.setText(report.getLocation().getName());
        reportComment.setText(report.getComment());

        if(report.getType() == Appconfig.CASE_REPORT){
            reportVictimWrapper.setVisibility(View.VISIBLE);
            reportVictim.setText(report.getVictim()+"");
        }else{
            reportVictimWrapper.setVisibility(View.GONE);
        }

        prefs = getSharedPreferences("com.mobilus.contagio", MODE_PRIVATE);
        if(prefs.getBoolean("sharefirstrun", true)) {
            ShowCaseViewHelper sv = new ShowCaseViewHelper();
            ToolbarActionItemTarget target = new ToolbarActionItemTarget(getToolbar(), R.id.action_share);
            sv.showToolbarAsTarget(ReportDetailActivity.this, target, getString(R.string.tutor_share_report), getString(R.string.tutor_desc_share_report), false, null);
        }

        images = report.getImages();
        if(images.size() > 0) {
            mImagesWrap.setVisibility(View.VISIBLE);
            mImagePager.setAdapter(new ReportImagePagerAdapter(images));
            mIndicator.setViewPager(mImagePager);
        }else{
            populateImages();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("sharefirstrun", true)) {
            prefs.edit().putBoolean("sharefirstrun", false).commit();
        }
    }

    private void populateImages() {
        if (!NetworkUtil.isConnected(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "No connection", Toast.LENGTH_LONG).show();
            return;
        }

        FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (result.get("status").getAsInt() > 0) {
                    mImagesWrap.setVisibility(View.VISIBLE);
                    report.deleteImages();
                    images.clear();
                    JsonArray resultArray = result.get("data").getAsJsonArray();

                    ReportImage image;
                    for (JsonElement jo : resultArray) {
                        image = new ReportImage(jo.getAsJsonObject(), report);
                        image.save();
                        images.add(image);
                    }

                    mImagePager.setAdapter(new ReportImagePagerAdapter(images));
                    mIndicator.setViewPager(mImagePager);
                }
            }
        };
        ReportImage.getList(this, report.getServerId(), callback);
    }

    private class ReportImagePagerAdapter extends PagerAdapter {
        private List<ReportImage> mImageList;
        private ArrayList<JSONObject> imgs = new ArrayList<>();

        public ReportImagePagerAdapter(List<ReportImage> images) {
            mImageList = images;

            for ( ReportImage image : mImageList) {
                JSONObject img = new JSONObject();
                try {
                    img.put("id", image.getServerId());
                    img.put("name", image.getName());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                imgs.add(img);
            }
        }

        @Override
        public int getCount() {
            return mImageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public View instantiateItem(ViewGroup container, final int position) {
            ImageView imgView = new ImageView(container.getContext());
            final ReportImage item = mImageList.get(position);

            Ion.with(imgView).load(Appconfig.REPORT_IMAGE_ASSET_URL + item.getName());
            imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), ViewImagesActivity.class);
                    i.putExtra("images", imgs.toString());
                    i.putExtra("position", position);
                    i.putExtra("itemType", Appconfig.NEWSFEED_REPORT);
                    v.getContext().startActivity(i);
                }
            });

            container.addView(imgView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return imgView;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
