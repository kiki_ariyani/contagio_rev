package com.mobilus.contagio.activity;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.adapter.ChooseImageAdapter;
import com.mobilus.contagio.adapter.PlacesAutoCompleteAdapter;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.helper.PlaceAPI;
import com.mobilus.contagio.model.Report;
import com.mobilus.contagio.model.ReportImage;
import com.mobilus.contagio.ui.ExpandableHeightGridView;
import com.mobilus.contagio.util.TextUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.mobilus.contagio.adapter.ChooseImageAdapter.OnItemEmpty;
import static android.widget.AdapterView.*;

/**
 * Created by user on 20/10/2015.
 */
public class ReportActivity extends BaseAppCompatActivity implements OnClickListener, OnItemClickListener {
    private final String[] types = Appconfig.REPORT_TYPES;
    private Report report;
    private com.mobilus.contagio.model.Location reportLocation;
    private Spinner type;
    private EditText date;
    private EditText comment;
    private EditText victim;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog datePickerDialog;
    private AutoCompleteTextView address;

    private View clickAreaDate;

    private Uri mImageCaptureUri;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    private ChooseImageAdapter reportImageAdapter;
    private ArrayList<JSONObject> reportImages = new ArrayList<>();
    final String STATUS_OK = "OK";

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for wifi status
    boolean isWifiEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60; // 1 minute
    private Location location; // location
    private String strAddress = "";

    private ProgressDialog dialog;
    private final String TAG = "ReportActivity";

    private PlacesAutoCompleteAdapter placesAdapter;
    private Double locationLat;
    private Double locationLng;
    private String apiId;
    private int apiType;
    private int serverId = 0;
    private int serverLocId = 0;

    private View btnPhotoLayout;
    private ImageButton altImgBtn;
    private View altbtnPhotoLayout;

    private int defaultType = Appconfig.CASE_REPORT;
    private Bundle intentBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            mImageCaptureUri = Uri.parse(savedInstanceState.getString("paramImageCaptureUri"));
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("New Report");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        intentBundle = getIntent().getExtras();

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        if(getIntent().hasExtra("type")){
            defaultType = getIntent().getIntExtra("type", Appconfig.CASE_REPORT);
        }

        initElements();
        setDateTimeField();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("paramImageCaptureUri", String.valueOf(mImageCaptureUri));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.form_report;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveLocation();
            return true;
        }else if(id == android.R.id.home){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void initElements() {
        btnPhotoLayout = findViewById(R.id.btn_photo_layout);
        altbtnPhotoLayout = findViewById(R.id.alt_btn_photo_layout);
        altImgBtn = (ImageButton) findViewById(R.id.alt_upload_image);

        buildImageDialog();

        ExpandableHeightGridView gridView = (ExpandableHeightGridView) findViewById(R.id.gridviewimg);
        reportImageAdapter = new ChooseImageAdapter(this, reportImages, false, Appconfig.REPORT_IMAGE_ASSET_URL);
        toogleAddImageBtn();

        gridView.setAdapter(reportImageAdapter);
        gridView.setExpanded(true);

        gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Intent i = new Intent(getApplicationContext(), ViewImagesActivity.class);
                i.putExtra("images", reportImages.toString());
                i.putExtra("position", position);
                i.putExtra("itemType", Appconfig.NEWSFEED_REPORT);
                startActivity(i);
            }
        });

        comment = (EditText) findViewById(R.id.report_comment);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, types);
        type = (Spinner) findViewById(R.id.report_type);
        type.setAdapter(adapter);

        date = (EditText) findViewById(R.id.report_date);
        date.setInputType(InputType.TYPE_NULL);

        address = (AutoCompleteTextView) findViewById(R.id.report_address);

        clickAreaDate = findViewById(R.id.click_area_report_date);

        Context context = this;
        getCoordinates(context);

        victim = (EditText) findViewById(R.id.report_victim);

        dialog = new ProgressDialog(ReportActivity.this);

        if(intentBundle != null){
            if(intentBundle.containsKey("report")) {
                initEdit();
            }else{
                type.setSelection(defaultType);
                if(getIntent().hasExtra("type")){
                    type.setEnabled(false);
                }
            }
        }

        type.setOnItemSelectedListener(new OnItemSelectedListener() {
               @Override
               public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                   if(position != 0){
                       victim.setVisibility(View.GONE);
                   }else{
                       victim.setVisibility(View.VISIBLE);
                   }
               }

               @Override
               public void onNothingSelected(AdapterView<?> parent) {

               }
           }
        );
    }

    private void setDateTimeField() {
        clickAreaDate.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        final int curYear = newCalendar.get(Calendar.YEAR);
        final int curMonth = newCalendar.get(Calendar.MONTH);
        final int curDate = newCalendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                if (year > curYear || (year == curYear && monthOfYear > curMonth) || (year == curYear && monthOfYear == curMonth && dayOfMonth > curDate)){

                    view.updateDate(curYear, curMonth, curDate);
                    newDate.set(curYear, curMonth, curDate);
                    date.setText(dateFormatter.format(newDate.getTime()));
                }
                else {
                    view.updateDate(year, monthOfYear, dayOfMonth);
                    newDate.set(year, monthOfYear, dayOfMonth);
                    date.setText(dateFormatter.format(newDate.getTime()));
                }

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }

    @Override
    public void onClick(View v) {
        datePickerDialog.show();
    }



    private void buildImageDialog() {
        final String[] items = new String[]{"From Camera", "From Gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment
                            .getExternalStorageDirectory(), "tmp_img_"
                            + String.valueOf(System.currentTimeMillis())
                            + ".jpg");
                    mImageCaptureUri = Uri.fromFile(file);

                    try {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                        intent.putExtra("return-data", true);
                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dialog.cancel();
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");

                    startActivityForResult(intent, PICK_FROM_FILE);
                }
            }
        });

        final AlertDialog dialog = builder.create();

        btnPhotoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }

        });
        altImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        } else {
            String path = "";

            if (requestCode == PICK_FROM_FILE) {
                mImageCaptureUri = data.getData();
                path = getRealPathFromURI(mImageCaptureUri); // from Gallery

                if (path == null)
                    path = mImageCaptureUri.getPath(); // from File Manager
            } else {
                if (mImageCaptureUri != null)
                    path = mImageCaptureUri.getPath(); // Get image from camera
            }

            // Declare new problem image
            if (path != "") {
                JSONObject img = new JSONObject();

                try {
                    img.put("id", "-1");
                    img.put("name", TextUtil.generateRandomString());
                    img.put("path", path);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                reportImages.add(img);
            }
            reportImageAdapter.notifyDataSetChanged();

            btnPhotoLayout.setVisibility(View.GONE);
            altbtnPhotoLayout.setVisibility(View.VISIBLE);
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);

        if (cursor == null)
            return null;

        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public Location getCoordinates(Context context) {
        if ( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        try {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            // getting wifi status
            isWifiEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                Toast.makeText(getApplicationContext(),
                        "Issue Location not found. No location provider is enabled", Toast.LENGTH_LONG)
                        .show();
            } else {
                this.canGetLocation = true;

                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                    Log.i("mylocation", "LOC Network Enabled");

                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if (location != null) {
                        Log.i("mylocation", "LOC by Network");
                      //  fillUpAddress(address, location);
                    }

                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                        Log.d("activity", "RLOC: GPS Enabled");
                        Log.i("mylocation", "RLOC: loc by GPS");
                     //   fillUpAddress(address, location);
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            Log.i("mylocation", "RLOC: loc by GPS");
                          //  fillUpAddress(address, location);
                        }
                    }
                }

                placesAdapter = new PlacesAutoCompleteAdapter(this, R.layout.item_place, location);
                address.setAdapter(placesAdapter);
                address.setOnItemClickListener(ReportActivity.this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.d("activity", "RLOC: Location xx "+latitude+" "+longitude);

        return location;
    }

    // Define a listener that responds to location updates
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location mLocation) {
         //   location = mLocation;
         //   fillUpAddress(address, location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private void fillUpAddress(final EditText editText, Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        editText.setHint(R.string.loading_address);
        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                + latitude + "," + longitude;
        Ion.with(this).load(url).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e != null) {
                    editText.setText(getString(R.string.unknown_location));
                    return;
                }
                String status = result.get("status").getAsString();
                if (status.equalsIgnoreCase(STATUS_OK)) {
                    JsonArray locationArray = result.get("results").getAsJsonArray();
                    JsonObject locationObject = locationArray.get(0).getAsJsonObject();
                    strAddress = locationObject.get("formatted_address").getAsString();
                    if (address.length() > 0) {
                        editText.setText(strAddress);
                    } else {
                        editText.setHint(R.string.address);
                    }
                }
            }
        });
    }

    private boolean save() {
        final boolean[] retval = {true};

        if (!comment.getText().toString().trim().isEmpty() && !address.getText().toString().trim().isEmpty()) {
            String strDate = date.getText().toString();
            Date dDate = null;
            try {
                dDate = dateFormatter.parse(strDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long dateInMili = dDate.getTime();


                long time= System.currentTimeMillis();
                int noOfVictim = (type.getSelectedItemPosition() == 0 ? Integer.parseInt(victim.getText().toString()) : 0);
                JsonObject json = new JsonObject();
                json.addProperty("id", serverId);
                json.addProperty("user_id", String.valueOf(Credential.getUser().getIdServer()));
                json.addProperty("location_id", reportLocation.getServerId());
                json.addProperty("type", type.getSelectedItemPosition());
                json.addProperty("comment", comment.getText().toString().trim());
                json.addProperty("date", dateInMili);
                json.addProperty("created_at", time);
                json.addProperty("updated_at", time);
                json.addProperty("victim", noOfVictim);
                json.addProperty("deleted_images", String.valueOf(reportImageAdapter.getDeletedImg()));

                Ion.with(this)
                        .load(Appconfig.REPORTS_API_URL + "save")
                        .setHeader(Credential.AUTH, Credential.AUTH_TYPE + Credential.getAuthToken())
                        .setJsonObjectBody(json)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e != null) {
                                    Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                                } else {
                                    if (result.get("status").getAsInt() == 1) {
                                        report = new Report(result.get("data").getAsJsonObject(), reportLocation);
                                        report.save();

                                        Log.i(TAG, "report images count : " + reportImages.size());

                                        if (reportImages.size() > 0) {
                                            retval[0] = saveImages(); //upload problem images
                                        }

                                        if(retval[0]){
                                            dialog.dismiss();
                                            showAlert(result.get("status").getAsInt(),result.get("msg").getAsString(), null);
                                        }
                                    }else {
                                        showAlert(result.get("status").getAsInt(),result.get("msg").getAsString(), null);
                                    }
                                }
                            }
                        });

        }else{
            if(comment.getText().toString().trim().equals("")){
                comment.setError("Comment is required");
            }
            if(address.getText().toString().trim().equals("")){
                address.setError("Address is required");
            }
        }
        return retval[0];
    }

    private boolean saveImages() {
        final boolean[] status = {true};
        long time= System.currentTimeMillis();
        final String[] message = new String[1];

        report.deleteImages();

        for (JSONObject jsonReportImage : reportImages) {
            ReportImage reportImage = new ReportImage();
            reportImage.setReport(report);
            reportImage.setServerId(jsonReportImage.optString("id"));
            reportImage.setName(jsonReportImage.optString("name"));
            reportImage.setPath(jsonReportImage.optString("path"));
            reportImage.setCreatedAt(time);
            reportImage.setUpdatedAt(time);

            reportImage.save();

            if (reportImage.getServerId() == "-1") {
                Ion.with(this)
                        .load(Appconfig.REPORTS_API_URL + "upload_image")
                        .setHeader(Credential.AUTH, Credential.AUTH_TYPE + Credential.getAuthToken())
                        .setMultipartParameter("id", reportImage.getServerId().toString())
                        .setMultipartParameter("report_id", report.getServerId().toString())
                        .setMultipartParameter("name", reportImage.getName().toString())
                        .setMultipartParameter("path",reportImage.getPath().toString())
                        .setMultipartParameter("created_at", String.valueOf(reportImage.getCreatedAt()))
                        .setMultipartParameter("updated_at", String.valueOf(reportImage.getUpdatedAt()))
                        .setMultipartFile("file", new File(reportImage.getPath()))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, final JsonObject result) {
                                if (e != null) {
                                    Log.i("SAVE_IMG_E", e.getMessage());
                                    message[0] = e.getMessage();
                                } else {
                                    if (result.get("status").getAsInt() == 1) {
                                        status[0] = status[0] && true;
                                    } else {
                                        status[0] = status[0] && false;
                                    }
                                    message[0] = result.get("msg").getAsString();
                                }
                            }
                        });
            }
        }

        return status[0];
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String str = placesAdapter.getResultData(position);
        int resultSource = placesAdapter.getResultSource();
        try {
            JSONObject locationData = new JSONObject(str);

            if(resultSource == Appconfig.SOURCE_FOURSQUARE){
                locationLat = locationData.getJSONObject("location").getDouble("lat");
                locationLng = locationData.getJSONObject("location").getDouble("lng");
                apiId = locationData.getString("id");
            }else{
                PlaceAPI mPlaceApi = new PlaceAPI();
                Location latLng = mPlaceApi.placesDetail(locationData.getString("place_id"));
                if(latLng != null){
                    locationLat = latLng.getLatitude();
                    locationLng = latLng.getLongitude();
                    apiId = locationData.getString("place_id");
                }
            }

            apiType = resultSource;
            serverLocId = 0;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean saveLocation() {
        final boolean[] status = {true};
        long time = System.currentTimeMillis();

        if (comment.getText().toString().trim().isEmpty() || address.getText().toString().trim().isEmpty()) {
            if(comment.getText().toString().trim().equals("")){
                comment.setError("Comment is required");
            }
            if(address.getText().toString().trim().equals("")){
                address.setError("Address is required");
            }
        }else{
            if(locationLat != null && locationLng != null){
                dialog.setMessage(getString(R.string.loading));
                dialog.show();

                JsonObject json = new JsonObject();
                json.addProperty("id", serverLocId);
                json.addProperty("name", address.getText().toString().trim());
                json.addProperty("lat", locationLat);
                json.addProperty("lng", locationLng);
                json.addProperty("api_id", apiId);
                json.addProperty("api_type", apiType);
                json.addProperty("created_at", time);
                Log.i("SAVE_LOCATION", json + "");
                Ion.with(this)
                        .load(Appconfig.LOCATIONS_API_URL + "save")
                        .setHeader(Credential.AUTH, Credential.AUTH_TYPE + Credential.getAuthToken())
                        .setJsonObjectBody(json)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e != null) {
                                    Log.i("SAVE_LOCATION_E", e.getMessage());
                                } else {
                                    if (result.get("status").getAsInt() == 1) {
                                        reportLocation = new com.mobilus.contagio.model.Location(result.get("data").getAsJsonObject());
                                        reportLocation.save();
                                        Log.i("SAVE_LOCATION", "result : " + result.get("msg").getAsString());
                                        //save report
                                        status[0] = save();
                                    } else {
                                        dialog.dismiss();
                                        Log.i("SAVE_LOCATION_E", result.get("msg").getAsString());
                                    }
                                }
                            }
                        });
            }else{
                address.setError("Location not found.");
            }
        }

        return status[0];
    }

    private void showAlert(final int status,String message, View.OnClickListener onClickListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(message)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                if (status == 1) {
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("report",new Gson().toJson(report));
                                    setResult(RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    ReportActivity.this.finish();
                                }
                            }
                        }).create();

        alertDialog.show();
    }

    private void initEdit(){
        String reportStr = getIntent().getStringExtra("report");
        report = new Gson().fromJson(reportStr, Report.class);

        List<ReportImage> listImages = report.getImages();

        for(int i=0; i < listImages.size();i++){
            JSONObject itemImg = new JSONObject();
            try {
                itemImg.put("id", listImages.get(i).getServerId());
                itemImg.put("name", listImages.get(i).getName());
                itemImg.put("path", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            reportImages.add(itemImg);
        }
        reportImageAdapter.notifyDataSetChanged();

        if(reportImages.size() > 0){
            altbtnPhotoLayout.setVisibility(View.VISIBLE);
            btnPhotoLayout.setVisibility(View.GONE);
        }

        address.setText(report.getLocation().getName());
        comment.setText(report.getComment());
        type.setSelection(report.getType());
        date.setText(dateFormatter.format(report.getDate()));

        if(report.getType() == 0){
            victim.setVisibility(View.VISIBLE);
            victim.setText(String.valueOf(report.getVictim()));
        }else{
            victim.setVisibility(View.GONE);
        }

        locationLat = Double.valueOf(report.getLocation().getLat());
        locationLng = Double.valueOf(report.getLocation().getLng());
        apiId = report.getLocation().getApiId();
        serverId = Integer.parseInt(report.getServerId());
        serverLocId = Integer.parseInt(report.getLocation().getServerId());
    }

    private void toogleAddImageBtn(){
        reportImageAdapter.setOnItemEmpty(new OnItemEmpty() {
            @Override
            public void onItemEmpty() {
                altbtnPhotoLayout.setVisibility(View.GONE);
                btnPhotoLayout.setVisibility(View.VISIBLE);
            }
        });
    }
}

