package com.mobilus.contagio.activity;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.authentication.AccountGeneral;
import com.mobilus.contagio.model.Preference;


public class InitActivity extends AppCompatActivity {
    private String TAG = InitActivity.class.getSimpleName();
    private AccountManager mAccountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.acitivity_init);
        ActionBar actionBar = getActionBar();

        if (actionBar != null)
            actionBar.hide();

        mAccountManager = AccountManager.get(this);

        getTokenForAccountCreateIfNeeded(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS);
    }

    /**
     * Get an auth token for the account.
     * If not exist - add it and then return its auth token.
     * If one exist - return its auth token.
     *
     * @param accountType
     * @param authTokenType
     */
    private void getTokenForAccountCreateIfNeeded(String accountType, String authTokenType) {
        final AccountManagerFuture<Bundle> future = mAccountManager.getAuthTokenByFeatures(accountType, authTokenType, null, this, null, null,
                new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        Bundle bnd = null;
                        try {
                            if (future.isCancelled()) {
                                // Do whatever you want. I understand that you want to close this activity,
                                // so supposing that mActivity is your activity:
                                finish();
                                return;
                            }

                            bnd = future.getResult();
                            final String authtoken = bnd.getString(AccountManager.KEY_AUTHTOKEN);

                            Log.d(TAG, "GetTokenForAccount Bundle is " + bnd);

                            if (authtoken != null) {
                                AccountGeneral.initLoggedInUser(InitActivity.this);
                                AccountGeneral.initExistingAuthToken(InitActivity.this);
                                goToMainActivity();
                            } else{
                                finish();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            showMessage(e.getMessage());
                        }
                    }
                }
                , null);
    }

    private void showMessage(final String msg) {
        if (TextUtils.isEmpty(msg))
            return;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void goToMainActivity() {
        Intent intent = new Intent(getApplication(), NewsfeedActivity.class);
        startActivity(intent);
        finish();
    }

}
