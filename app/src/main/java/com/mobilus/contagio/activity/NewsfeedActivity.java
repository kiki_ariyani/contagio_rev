package com.mobilus.contagio.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.mobilus.contagio.R;
import com.mobilus.contagio.adapter.NewsFeedAdapter;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.helper.ShowCaseViewHelper;
import com.mobilus.contagio.model.Newsfeed;
import com.mobilus.contagio.ui.EndlessRecyclerOnScrollListener;
import com.mobilus.contagio.util.LocationUtil;
import com.mobilus.contagio.util.NetworkUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Fikran on 12 Okt 2015.
 */
public class NewsfeedActivity extends DrawerActivity {
    private static final String TAG = "NewsfeedActivity";
    private static final String SORT_BY_DISTANCE = "distance";
    private static final String SORT_BY_CREATED_TIME = "created_at desc";
    private final String[] sorts = {"Distance","Created Time"};
    private final CharSequence[] filterTypes = {"Location", "Cases Report", "Breeding Ground Report", "Fogging Report", "Event"};
    boolean bl[] = new boolean[filterTypes.length];
    private ArrayList selectedFilter = new ArrayList(){{ add(-1); add(0); add(1); add(2); add(3); add(4); }};

    private RecyclerView mNewsFeedList;
    private RecyclerView.Adapter mNewsFeedAdapter;
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar mProgress;
    private List<Newsfeed> newsFeed;

    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean isWifiEnabled = false;
    boolean canGetLocation = false;
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    private Location location;

    private final int LOCATION_ACCESS = 11;
    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarTitle("News Feed");

        getCoordinates();
        Arrays.fill(bl, Boolean.TRUE); //set default filter all checked

        if ( Build.VERSION.SDK_INT >= 23 ){ // Tell user with Marshmallow OS to enabled location permission
            LocationUtil.enablingLocationPermissionMarshmallow(this);
        }

        prefs = getSharedPreferences("com.mobilus.contagio", MODE_PRIVATE);
        ShowCaseViewHelper sv = new ShowCaseViewHelper();
        ViewTarget nextTarget = new ViewTarget(R.id.fam, this);
        if (Credential.getUser() != null) {
            if(prefs.getBoolean("newsfeedloginfirstrun", true)) {
                floatingBtn.open(false);
                sv.show(NewsfeedActivity.this, null, getString(R.string.tutor_newsfeed), getString(R.string.tutor_desc_newsfeed), null, nextTarget, getString(R.string.tutor_report), getString(R.string.tutor_desc_report), "CenterRight", true);
            }
        } else {
            if(prefs.getBoolean("newsfeedfirstrun", true)) {
                sv.show(NewsfeedActivity.this, null, getString(R.string.tutor_newsfeed), getString(R.string.tutor_desc_newsfeed), false, null);
            }
        }
        initActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (prefs.getBoolean("newsfeedfirstrun", true)) {
            prefs.edit().putBoolean("newsfeedfirstrun", false).commit();
        }

        if (Credential.getUser() != null) {
            if (prefs.getBoolean("newsfeedloginfirstrun", true)) {
                prefs.edit().putBoolean("newsfeedloginfirstrun", false).commit();
            }
        }
    }

    public void initActivity(){
        if(!isNetworkEnabled) { // Prevent user to enabled Location access
            LocationUtil.displayPromptForEnablingGPS(this);
        }else{
            if (location != null){
                initElements();

                if (!NetworkUtil.isConnected(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), R.string.no_connection, Toast.LENGTH_LONG).show();
                    mProgress.setVisibility(View.GONE);
                } else {
                    populateList(SORT_BY_DISTANCE);
                }

                mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        populateList(SORT_BY_DISTANCE);
                    }
                });

                mNewsFeedList.setOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
                    @Override
                    public void onLoadMore(int current_page) {
                        loadMoreList();
                    }
                });
            }else{
                Toast.makeText(this, getString(R.string.failed_get_location), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void initElements(){
        mNewsFeedList = (RecyclerView) findViewById(R.id.newsfeed_list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        mNewsFeedList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mNewsFeedList.setLayoutManager(mLayoutManager);

        newsFeed = Newsfeed.listAll(Newsfeed.class);
        mNewsFeedAdapter = new NewsFeedAdapter(newsFeed);
        mNewsFeedList.setAdapter(mNewsFeedAdapter);
    }

    protected void populateList(String sortBy) {
        mSwipeRefreshLayout.setRefreshing(true);

        if (!NetworkUtil.isConnected(this)) {
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getApplicationContext(), R.string.no_connection, Toast.LENGTH_LONG).show();
            return;
        }

        final FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                mProgress.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);

                try{
                    if (result.get("status").getAsInt() == 0) {
                        Toast.makeText(getApplicationContext(), result.get("msg").getAsString(),
                                Toast.LENGTH_LONG).show();
                    } else {
                        JsonArray data = result.get("data").getAsJsonArray();

                        Newsfeed.deleteAll(Newsfeed.class);
                        newsFeed.clear();

                        for (JsonElement jo : data) {
                            Newsfeed newsfeed = new Newsfeed(jo.getAsJsonObject());
                            newsfeed.save();
                            newsFeed.add(newsfeed);
                        }

                        if (data.size() >= Integer.parseInt(Appconfig.FETCH_LIMIT)) {
                            mNewsFeedList.setOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
                                @Override
                                public void onLoadMore(int current_page) {
                                    loadMoreList();
                                }
                            });
                        }

                        mNewsFeedAdapter.notifyDataSetChanged();
                    }
                }catch (NullPointerException n){}
            }
        };
        Newsfeed.getList(this, callback, location, sortBy, selectedFilter);
    }

    protected void loadMoreList(){
        if (!NetworkUtil.isConnected(this)) {
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getApplicationContext(), R.string.no_connection,
                    Toast.LENGTH_LONG).show();
            return;
        }

        final FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                // Stop the refreshing indicator
                mSwipeRefreshLayout.setRefreshing(false);

                if (result.get("status").getAsInt() == 0) {
                    /*Toast.makeText(getApplicationContext(), result.get("msg").getAsString(),
                            Toast.LENGTH_LONG).show();*/
                } else {
                    JsonArray data = result.get("data").getAsJsonArray();

                    for (JsonElement jo : data) {
                        Newsfeed newsfeed = new Newsfeed(jo.getAsJsonObject());
                        newsfeed.save();
                        newsFeed.add(newsfeed);
                    }

                    if (data.size() >= Integer.parseInt(Appconfig.FETCH_LIMIT)) {
                        mNewsFeedList.setOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
                            @Override
                            public void onLoadMore(int current_page) {
                                loadMoreList();
                            }
                        });
                    }

                    mNewsFeedAdapter.notifyDataSetChanged();
                }
            }
        };
        Newsfeed.getMore(this, newsFeed.size(), callback, location, SORT_BY_DISTANCE, selectedFilter);
    }

    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location mLocation) {
            location = mLocation;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public Location getCoordinates() {
        if ( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        try {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            // getting wifi status
            isWifiEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                Log.d(TAG, "GPS or Netword Disabled");
            } else {
                this.canGetLocation = true;

                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                    Log.i("mylocation", "LOC Network Enabled");
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if (location != null) {
                        Log.i("mylocation", "LOC by Network");
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES,
                                locationListener);
                        Log.d("activity", "RLOC: GPS Enabled");
                        Log.i("mylocation", "RLOC: loc by GPS");
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            Log.i("mylocation", "RLOC: loc by GPS");
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    private void restartActivity(){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void showFilterDialog(){
        new AlertDialog.Builder(NewsfeedActivity.this)
                .setTitle(R.string.filter)
                .setMultiChoiceItems(filterTypes, bl, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        int index = which - 1;
                        if(isChecked){
                            selectedFilter.add(index);
                        } else if (selectedFilter.contains(index)) {
                            selectedFilter.remove(Integer.valueOf(index));
                        }
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        populateList(SORT_BY_DISTANCE);
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_newsfeed;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_newsfeed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.menuSortDistance:
                mProgress.setVisibility(View.VISIBLE);
                populateList(SORT_BY_DISTANCE);
                return true;

            case R.id.menuSortCreatedTime:
                mProgress.setVisibility(View.VISIBLE);
                populateList(SORT_BY_CREATED_TIME);
                return true;*/

            case R.id.menu_filter:
                showFilterDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            restartActivity();
        } else {
            this.finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == LOCATION_ACCESS){
            restartActivity();
        }
    }
}
