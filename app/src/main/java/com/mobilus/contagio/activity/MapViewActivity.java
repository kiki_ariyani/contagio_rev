package com.mobilus.contagio.activity;

import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.google.android.gms.maps.GoogleMap;
import com.mobilus.contagio.adapter.LocationLatestUpdateAdapter;
import com.mobilus.contagio.adapter.PlacesAutoCompleteAdapter;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.helper.ContentShare;
import com.mobilus.contagio.helper.ShowCaseViewHelper;
import com.mobilus.contagio.model.Location;
import com.mobilus.contagio.util.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Target;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Fikran on 12 Okt 2015.
 */
public class MapViewActivity extends DrawerActivity implements OnMapReadyCallback, SearchView.OnQueryTextListener {
    List<Location> listLocation;
    private Map<Marker, Location> allMarkersMap = new HashMap<>();
    private RecyclerView latestUpdatesList;
    private LocationLatestUpdateAdapter mLuAdapter;
    private ProgressBar progressBar;
    private SearchView searchView;
    private GoogleMap map;
    private ProgressDialog progressDialog;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for wifi status
    boolean isWifiEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60; // 1 minute
    private android.location.Location location; // location
    private int iId = 0;
    private int iType = Appconfig.NEWSFEED_REPORT;
    private int caseReportNum = 0;
    private int breadingReportNum = 0;
    private int foggingReportNum = 0;
    private int bittenReportNum = 0;

    SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            if(extras.containsKey("newsfeed_id")){
                iId = getIntent().getIntExtra("newsfeed_id", 0);
                iType = getIntent().getIntExtra("newsfeed_type", Appconfig.NEWSFEED_REPORT);
            }
        }else{
            Toast tGetLocation = Toast.makeText(getApplicationContext(), R.string.getting_user_location, Toast.LENGTH_LONG);
            tGetLocation.show();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        searchView = (SearchView) findViewById(R.id.search_location);
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();
        searchView.setFocusable(false);
        searchView.setOnQueryTextListener(MapViewActivity.this);
        progressDialog = new ProgressDialog(MapViewActivity.this);
        progressDialog.setMessage(getString(R.string.loading));

        prefs = getSharedPreferences("com.mobilus.contagio", MODE_PRIVATE);

        setActionBarTitle("Map View");

        attachFloatingActionMenu();

        if(prefs.getBoolean("mapfirstrun", true)){
            ShowCaseViewHelper sv = new ShowCaseViewHelper();
            ViewTarget nextTarget = new ViewTarget(R.id.container_search,this);
            sv.show(MapViewActivity.this, null, getString(R.string.tutor_mapview), getString(R.string.tutor_desc_mapview),null,nextTarget, getString(R.string.tutor_search), getString(R.string.tutor_desc_search),"CenterRight", true);}
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("mapfirstrun", true)) {
            prefs.edit().putBoolean("mapfirstrun", false).commit();
        }
    }

    private void attachFloatingActionMenu(){
        if (Credential.getUser() == null) {
            return;
        }
        ViewGroup containerMap = (ViewGroup)findViewById(R.id.container_map);
        containerMap.addView(getDefaultFloatingActionMenu(containerMap));
    }

    @Override
    protected int getContentView() {
        return R.layout.map_view;
    }

    @Override
    protected boolean isDefaultFloatingActionMenuAttached() {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        progressBar.setVisibility(View.GONE);
        listLocation = Location.listAll(Location.class);
        map = googleMap;
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(3.1393364, 101.5467663), 12.0f));

        if (!NetworkUtil.isConnected(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), R.string.no_connection, Toast.LENGTH_LONG).show();
        }else if(iId != 0){
            populateMarker(map, "", true);
        }else {
            populateMarker(map, "", false);
        }
    }

    protected void populateMarker(final GoogleMap googleMap, final String keyword, final boolean isFromNewsfeed) {
        android.location.Location coordinate = getCoordinates(MapViewActivity.this);
        final String mKeyword = keyword;
        final LatLng focusLatLng = new LatLng(coordinate.getLatitude(), coordinate.getLongitude());
        String mUrl;

        if(isFromNewsfeed){
            mUrl = Appconfig.LOCATIONS_API_URL + "get_newsfeed_item/";
        }else{
            mUrl = Appconfig.LOCATIONS_API_URL + "get_list/" + keyword;
            iType = Appconfig.NEWSFEED_REPORT;
            iId = 0;
        }

        Ion.with(this)
                .load(mUrl)
                .setBodyParameter("type", String.valueOf(iType))
                .setBodyParameter("id", String.valueOf(iId))
                .setBodyParameter("lat", String.valueOf(coordinate.getLatitude()))
                .setBodyParameter("lng", String.valueOf(coordinate.getLongitude()))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                        } else {
                            googleMap.clear();

                            if (result.get("status").getAsInt() == 1) {
                                JsonArray resultData = result.get("data").getAsJsonArray();

                                if (resultData.size() > 0) {
                                    Location.deleteAll(Location.class);
                                    listLocation.clear();

                                    for (JsonElement rLocation : resultData) {
                                        Location location = new Location(rLocation.getAsJsonObject());
                                        location.save();
                                        listLocation.add(location);
                                    }

                                    //Set map focus
                                    if (mKeyword != "" || isFromNewsfeed) {
                                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.valueOf(listLocation.get(0).getLat()), Double.valueOf(listLocation.get(0).getLng())), 12.0f));
                                    }else {
                                        Marker defaultMarker = googleMap.addMarker(new MarkerOptions().position(focusLatLng).title("You are here").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                                        defaultMarker.showInfoWindow();
                                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(focusLatLng, 14.0f));
                                    }
                                    Log.i("LOCATION_DB_NUM", Location.count(Location.class) + "");
                                }

                                for (int i = 0; i < listLocation.size(); i++) {
                                    Double lat = Double.valueOf(listLocation.get(i).getLat());
                                    Double lng = Double.valueOf(listLocation.get(i).getLng());
                                    String name = listLocation.get(i).getName();
                                    LatLng place = new LatLng(lat, lng);
                                    Marker lMarker = googleMap.addMarker(new MarkerOptions().position(place).title(name));
                                    allMarkersMap.put(lMarker, listLocation.get(i));
                                }

                                searchView.clearFocus();

                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                            }else{
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }

                                String noResultMsg = (keyword != "" ? "\""+keyword+ "\" not found" : "Data not found");
                                Toast toast = Toast.makeText(MapViewActivity.this, noResultMsg, Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toast.show();
                            }
                        }
                    }
                });

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                Location markerData = allMarkersMap.get(marker);

                if(markerData != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MapViewActivity.this);
                    LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = layoutInflater.inflate(R.layout.map_marker_detail, null);
                    JsonArray latestUpdates = markerData.getLatestUpdates();

                    int updatesThisWeek = 0;

                    for (int i = 0; i < latestUpdates.size(); i++) {
                        JsonObject lastUpdate = latestUpdates.get(i).getAsJsonObject();
                        //count each report number by report type
                        int lastUpdateType = lastUpdate.get("report_type").getAsInt();
                        if(lastUpdateType == Appconfig.CASE_REPORT){
                            caseReportNum++;
                        }else if(lastUpdateType == Appconfig.BREADING_REPORT){
                            breadingReportNum++;
                        }else if(lastUpdateType == Appconfig.FOGGING_REPORT){
                            foggingReportNum++;
                        }/*else if(lastUpdateType == Appconfig.BITTEN_REPORT){
                            bittenReportNum++;
                        }*/

                        //count update this week
                        Date updateDate = new Date(lastUpdate.get("created_at").getAsLong());
                        if(isDateInCurrentWeek(updateDate)){
                            updatesThisWeek++;
                        }
                    }

                    TextView locationName = (TextView) view.findViewById(R.id.location_name);
                    locationName.setText(marker.getTitle());

                    TextView locationDesc = (TextView) view.findViewById(R.id.location_desc);

                    final ProgressBar mProgressBar = (ProgressBar) view.findViewById(R.id.progress);

                    ImageView coverImage = (ImageView) view.findViewById(R.id.cover_img);
                    if (markerData.getCoverImage() != "") {
                        Ion.with(coverImage).load(Appconfig.APP_URL + markerData.getCoverImage()).setCallback(new FutureCallback<ImageView>() {

                            @Override
                            public void onCompleted(Exception e, ImageView result) {
                                mProgressBar.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                        coverImage.setVisibility(View.GONE);
                    }

                    String strLocationDesc;
                    if(updatesThisWeek > 0){
                        strLocationDesc = updatesThisWeek + " update" + (updatesThisWeek > 1 ? "s" : "") + " this week";
                    }else{
                        strLocationDesc = "Last Updated : "+DateFormat.format("MMMM dd, yyyy", new Date(latestUpdates.get(0).getAsJsonObject().get("created_at").getAsLong()));
                    }
                    locationDesc.setText(strLocationDesc);

                    latestUpdatesList = (RecyclerView) view.findViewById(R.id.latest_update_list);
                    latestUpdatesList.setLayoutManager(new LinearLayoutManager(MapViewActivity.this));

                    mLuAdapter = new LocationLatestUpdateAdapter(latestUpdates, getApplicationContext(), markerData);
                    latestUpdatesList.setAdapter(mLuAdapter);

                    ImageButton btnShare = (ImageButton) view.findViewById(R.id.btn_share);
                    btnShare.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ContentShare shareIntent = new ContentShare();
                            shareIntent.share(MapViewActivity.this, "Contageo Shared Location", "Contageo App report on \nLocation : "+marker.getTitle()+"\nCase Report : "+caseReportNum+"\nBreeding Report : "+breadingReportNum+"\nFogging Report : "+foggingReportNum+"\nBitten Report :"+bittenReportNum);
                        }
                    });

                    builder.setView(view);
                    builder.show();
                }else{
                    marker.showInfoWindow();
                }

                return true;
            }
        });
    }

    public android.location.Location getCoordinates(Context context) {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        try {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            // getting wifi status
            isWifiEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                Toast.makeText(getApplicationContext(),
                        "Issue Location not found. No location provider is enabled", Toast.LENGTH_LONG)
                        .show();
            } else {
                this.canGetLocation = true;

                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                    Log.i("mylocation", "LOC Network Enabled");

                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if (location != null) {
                        Log.i("mylocation", "LOC by Network");
                    }

                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                        Log.d("activity", "RLOC: GPS Enabled");
                        Log.i("mylocation", "RLOC: loc by GPS");
                        //   fillUpAddress(address, location);
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    // Define a listener that responds to location updates
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(android.location.Location mLocation) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public boolean onQueryTextSubmit(String query) {
        progressDialog.show();
        populateMarker(map, query, false);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText.length() == 0){
            progressDialog.show();
            populateMarker(map, "", false);
        }
        return false;
    }

    private boolean isDateInCurrentWeek(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);
        return week == targetWeek && year == targetYear;
    }
}
