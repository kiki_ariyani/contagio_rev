package com.mobilus.contagio.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.authentication.AccountGeneral;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.mobilus.contagio.model.Preference;
import com.mobilus.contagio.model.User;
import com.mobilus.contagio.service.NotificationService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private static final int DRAWER_TRANSITION_DELAY = 400;
    private boolean mShouldFinish = true;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    ViewGroup appBarRoot;
    private AlertDialog reportTypeMenuDialog;
    private NavigationView navigationView;
    private int selectedNavigationItemId = 0;
    FloatingActionMenu floatingBtn;
    ProfilePictureView profilePictureView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(getToolbar());

        appBarRoot = (ViewGroup)findViewById(R.id.app_bar_root);

        if(isDefaultFloatingActionMenuAttached()){
            attachDefaultFloatingActionMenu();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ViewGroup container = (ViewGroup) findViewById(R.id.activity_container);
        container.addView(getLayoutInflater().inflate(getContentView(), null));

        selectedNavigationItemId = getIntent().getIntExtra("selected_navigation_item_id", 0);
        if(selectedNavigationItemId == 0){
            selectedNavigationItemId = R.id.nav_newsfeed;
        }

        navigationView.setCheckedItem(selectedNavigationItemId);

        ImageView userAvatar = (ImageView) findViewById(R.id.account_image);
        TextView userName = (TextView) findViewById(R.id.account_name);
        TextView userEmail = (TextView) findViewById(R.id.account_email);
        profilePictureView = (ProfilePictureView)findViewById(R.id.fb_image);

        if(Credential.getUser() != null) {
            userName.setText(Credential.getUser().getName());
            userEmail.setText(Credential.getUser().getEmail());

            if(!Credential.getUser().getFbId().equals("")){
                userAvatar.setVisibility(View.GONE);
                profilePictureView.setProfileId(Credential.getUser().getFbId());
            }else{
                profilePictureView.setVisibility(View.GONE);
            }
        }else{
            View containerSignedHeader = findViewById(R.id.container_signed_header);
            View containerUnsignedHeader = findViewById(R.id.container_unsigned_header);
            MenuItem logoutMenu = navigationView.getMenu().findItem(R.id.nav_logout);

            containerSignedHeader.setVisibility(View.GONE);
            containerUnsignedHeader.setVisibility(View.VISIBLE);
            logoutMenu.setVisible(false);

            containerUnsignedHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Preference.set(Preference.KEY__IS_LOGIN_SKIPPED, false);

                    Intent intent = new Intent(getApplication(), InitActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }

    protected void attachFloatingActionMenu(View mFloatingActionMenu){
        appBarRoot.addView(mFloatingActionMenu);
    }

    protected boolean isDefaultFloatingActionMenuAttached(){
        return true;
    }

    protected void attachDefaultFloatingActionMenu(){
        if (Credential.getUser() == null) {
            return;
        }
        attachFloatingActionMenu(getDefaultFloatingActionMenu(appBarRoot));
    }

    protected View getDefaultFloatingActionMenu(){
        return getDefaultFloatingActionMenu(null);
    }

    protected View getDefaultFloatingActionMenu(ViewGroup root){
        LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.floating_action_menu, root, false);

        final FloatingActionMenu fam = (FloatingActionMenu)view.findViewById(R.id.fam);
        fam.setClosedOnTouchOutside(true);
        floatingBtn = fam;

        FloatingActionButton fabNewReport = (FloatingActionButton)view.findViewById(R.id.fab_new_report);
        FloatingActionButton fabNewEvent = (FloatingActionButton)view.findViewById(R.id.fab_new_event);

        fabNewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fam.close(false);
                final AlertDialog.Builder builder = new AlertDialog.Builder(DrawerActivity.this);
                LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.menu_report_type, null);

                ViewGroup reportFoggingBtn = (ViewGroup) view.findViewById(R.id.report_fogging_btn);
                reportFoggingBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openReportForm(Appconfig.FOGGING_REPORT);
                    }
                });

                ViewGroup reportCaseBtn = (ViewGroup) view.findViewById(R.id.report_case_btn);
                reportCaseBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openReportForm(Appconfig.CASE_REPORT);
                    }
                });

                ViewGroup reportBreedingBtn = (ViewGroup) view.findViewById(R.id.report_breeding_btn);
                reportBreedingBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openReportForm(Appconfig.BREADING_REPORT);
                    }
                });

            /*    ViewGroup reportBiteBtn = (ViewGroup) view.findViewById(R.id.report_bite_btn);
                reportBiteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openReportForm(Appconfig.BITTEN_REPORT);
                    }
                });*/

                builder.setView(view);
                reportTypeMenuDialog = builder.show();
            }
        });

        fabNewEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fam.close(false);
                Intent intent = new Intent(getApplication(), EventActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }


    protected void setActionBarTitle(String title){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
    }

    protected int getContentView(){
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    protected void createMenuDrawer(int contentViewId) {
        ViewGroup container = (ViewGroup) findViewById(R.id.activity_container);
        container.addView(getLayoutInflater().inflate(contentViewId, null));
        initMenuDrawer();
    }

    protected Toolbar getToolbar() {
        if (toolbar == null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
        }

        return toolbar;
    }

    private void initMenuDrawer() {
        if (drawer != null) {
            mDrawerToggle = new ActionBarDrawerToggle(
                    this, drawer, getToolbar(),
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close
            ) {
                public void onDrawerClosed(View view) {
                    invalidateOptionsMenu();
                }

                public void onDrawerOpened(View drawerView) {
                    invalidateOptionsMenu();
                }
            };
            drawer.setDrawerListener(mDrawerToggle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            mDrawerToggle.syncState();
        }
    }

    protected void openDrawer() {
        if (drawer != null) {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    private void toggleDrawer() {
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                closeDrawer();
            } else {
                openDrawer();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        Intent initIntent = null;

        if (id == R.id.nav_mapview) {
            initIntent = new Intent(this, MapViewActivity.class);
        } else if (id == R.id.nav_newsfeed) {
            initIntent = new Intent(this, NewsfeedActivity.class);
        }/* else if (id == R.id.nav_contact_us) {
            initIntent = new Intent(this, ContactUsActivity.class);
        }*/
        else if(id == R.id.nav_graph){
            initIntent = new Intent(this, ReportGraphActivity.class);
        }
        else if (id == R.id.nav_logout) {
            navigationView.setCheckedItem(selectedNavigationItemId);
            showAlertLogout();
            return true;
        }

        closeDrawer();

        if(initIntent == null){
            return true;
        }

        initIntent.putExtra("selected_navigation_item_id", id);

        if(getClass().getName().equals(initIntent.getComponent().getClassName())){
            return true;
        }

        if (mShouldFinish && initIntent != null) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(item.getTitle());
            }
            final Intent intent = initIntent;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(0, 0);
                }
            }, DRAWER_TRANSITION_DELAY);
        }else{

        }

//        closeDrawer();
        return true;
    }

    private void closeDrawer(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    public class LogoutTask extends AsyncTask<String, Void, Void> {
        public LogoutTask(Activity activity) {
            this.activity = activity;
            this.dialog = new ProgressDialog(activity);
            this.dialog.setMessage(getString(R.string.logging_out));
        }

        private ProgressDialog dialog;  // progress dialog to show user that the backup is processing.
        private Activity activity; // application context

        @Override
        protected Void doInBackground(String... params) {
            /** Move last authToken to SharedPreferences **/
            SharedPreferences prefs = activity.getSharedPreferences(
                    getString(R.string.package_name), Context.MODE_PRIVATE);

            prefs.edit().putString(getString(R.string.package_name) + getString(R.string.preference_last_token),
                    Credential.getAuthToken()).apply();

            /** Reset Database **/
            Appconfig.resetDB(getApplicationContext());

            /** Remove account **/
            AccountGeneral.removeAccount(DrawerActivity.this);

            LoginManager.getInstance().logOut();

            Credential.reset();

            /** Stop Notification Service **/
            stopService(new Intent(getBaseContext(), NotificationService.class));
            return null;
        }

        protected void onPreExecute() {
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(Void mVoid) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            finishLogout();
        }
    }

    private void finishLogout(){
        Intent intent = new Intent(getApplication(), InitActivity.class);
        startActivity(intent);
        finish();
    }

    private void showAlertLogout() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setTitle(R.string.alert_dialog_title_logout)
                .setPositiveButton(R.string.confirm,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                new LogoutTask(DrawerActivity.this).execute();
                            }
                        })
                .setNegativeButton(R.string.cancel,
                        null).create();

        alertDialog.show();
    }

    private void openReportForm(int reportType){
        reportTypeMenuDialog.dismiss();
        Intent intent = new Intent(getApplication(), ReportActivity.class);
        intent.putExtra("type",reportType);
        startActivity(intent);
    }
}
