package com.mobilus.contagio.activity;

import android.os.Bundle;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.mobilus.contagio.R;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.model.Report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by user on 09/09/2016.
 */
public class ReportGraphActivity extends DrawerActivity {
    private BarChart barChart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarTitle("Report Graph");

        initBarChart();

        FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (result.get("status").getAsInt() > 0) {
                    JsonArray resultArray = result.get("data").getAsJsonArray();

                    ArrayList<BarEntry> entries = new ArrayList<>();
                    int i=0;
                    for (JsonElement jo : resultArray) {
                        entries.add(new BarEntry(Math.round(jo.getAsFloat()), i));
                        i++;}

                    BarDataSet dataset = new BarDataSet(entries, "Projects");

                    ArrayList<String> labels = new ArrayList<String>(Arrays.asList(Appconfig.REPORT_TYPES));
                    BarData data = new BarData(labels, dataset);
                    dataset.setColors(ColorTemplate.COLORFUL_COLORS);
                    barChart.setData(data);
                    barChart.invalidate();
                }
            }
        };
        Report.getGraphDataByType(this, callback);
    }

    @Override
    protected int getContentView() {
        return R.layout.report_graph;
    }

    private void initBarChart(){
        barChart = (BarChart) findViewById(R.id.bar_chart);

        BarData data = new BarData();
        barChart.setData(data);
    }
}

