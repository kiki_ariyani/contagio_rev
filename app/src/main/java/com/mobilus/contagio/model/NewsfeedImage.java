package com.mobilus.contagio.model;

import android.content.Context;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.config.Credential;
import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Desi on 29 Oct 02015.
 */
public class NewsfeedImage extends SugarRecord {
    private String serverId;
    private String name;
    private Long createdAt;
    private Long updatedAt;
    private int ownerId;
    private int type;

    private Newsfeed newsfeed;

    public NewsfeedImage(JsonObject jsonObject, Newsfeed newsfeed) {
        this.serverId = jsonObject.get("id").getAsString();
        this.name = jsonObject.get("name").getAsString();
        this.createdAt = jsonObject.get("created_at").getAsLong();
        this.updatedAt = jsonObject.get("updated_at").getAsLong();
        this.ownerId = newsfeed.getServerId();
        this.newsfeed = newsfeed;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Newsfeed getNewsfeed() {
        return newsfeed;
    }

    public void setNewsfeed(Newsfeed newsfeed) {
        this.newsfeed = newsfeed;
    }

    public static void getList(Context context, int serverId, int type, FutureCallback<JsonObject> callback) {
        Ion.with(context)
                .load(Appconfig.REPORTS_API_URL + "get_newsfeed_images/")
                .setBodyParameter("owner_id", serverId + "")
                .setBodyParameter("type", type + "")
                .asJsonObject()
                .setCallback(callback);
    }

}
