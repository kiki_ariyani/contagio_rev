package com.mobilus.contagio.model;

import android.content.Context;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.config.Appconfig;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by user on 22/10/2015.
 */
public class ReportImage extends SugarRecord implements Serializable {
    private final String TAG = "ReportImage";

    private String serverId;
    private String reportId;
    private String name;
    private Long createdAt;
    private Long updatedAt;
    private String path;

    private Report report;

    public ReportImage(){

    }

    public ReportImage(JsonObject jsonObject, Report report) {
        this.serverId = jsonObject.get("id").getAsString();
        this.reportId = jsonObject.get("report_id").getAsString();
        this.name = jsonObject.get("name").getAsString();
        this.createdAt = jsonObject.get("created_at").getAsLong();
        this.updatedAt = jsonObject.get("updated_at").getAsLong();
        this.report = report;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public static void getList(Context context, String id, FutureCallback<JsonObject> callback) {
        Ion.with(context)
                .load(Appconfig.REPORTS_API_URL + "get_images/")
                .setBodyParameter("report_id", id)
                .asJsonObject()
                .setCallback(callback);
    }
}
