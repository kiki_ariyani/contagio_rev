package com.mobilus.contagio.model;

import android.content.Context;
import android.location.Location;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.config.Appconfig;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desi on 21 Oct 02015.
 */
public class Newsfeed extends SugarRecord {
    private int serverId;
    private int userId;

    private int reportType;
    private String address;
    private double lat;
    private double lng;
    private String comment;
    private Long date;

    private Long createdAt;
    private Long updatedAt;

    private String eventName;
    private String eventCreator;
    private Long datetime;
    private String organizer;

    private int caseNumWeek;
    private int victims;
    private Long lastCaseDate;

    private double distance;
    private int type;

    public Newsfeed(JsonObject jsonObject) {
        this.serverId = jsonObject.get("id").getAsInt();
        this.userId = jsonObject.get("user_id").getAsInt();
        this.reportType = jsonObject.get("report_type").getAsInt();
        this.address = jsonObject.get("address").getAsString();
        this.lat = jsonObject.get("lat").getAsDouble();
        this.lng = jsonObject.get("lng").getAsDouble();
        this.comment = jsonObject.get("comment").getAsString();
        this.date = jsonObject.get("date").getAsLong();
        this.createdAt = jsonObject.get("created_at").getAsLong();
        this.updatedAt = jsonObject.get("updated_at").getAsLong();
        this.eventName = jsonObject.get("event_name").getAsString();
        this.eventCreator = jsonObject.get("creator").getAsString();
        this.datetime = jsonObject.get("datetime").getAsLong();
        this.organizer = jsonObject.get("organizer").getAsString();
        this.caseNumWeek = jsonObject.get("case_num_week").getAsInt();
        this.victims = jsonObject.get("victims").getAsInt();
        this.lastCaseDate = jsonObject.get("last_case_date").getAsLong();
        this.distance = jsonObject.get("distance").getAsDouble();
        this.type = jsonObject.get("type").getAsInt();
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventCreator() {
        return eventCreator;
    }

    public void setEventCreator(String eventCreator) {
        this.eventCreator = eventCreator;
    }

    public Long getDatetime() {
        return datetime;
    }

    public void setDatetime(Long datetime) {
        this.datetime = datetime;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public int getCaseNumWeek() {
        return caseNumWeek;
    }

    public void setCaseNumWeek(int caseNumWeek) {
        this.caseNumWeek = caseNumWeek;
    }

    public int getVictims() {
        return victims;
    }

    public void setVictims(int victims) {
        this.victims = victims;
    }

    public Long getLastCaseDate() {
        return lastCaseDate;
    }

    public void setLastCaseDate(Long lastCaseDate) {
        this.lastCaseDate = lastCaseDate;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public static void getList(Context context, FutureCallback<JsonObject> callback,
                               Location location, String sort, ArrayList<Integer> filters) {
        String filterStr = "";
        for (Integer i : filters) {
            filterStr += i + (i != filters.get(filters.size() - 1) ? "," : "");
        }

        Ion.with(context)
                .load(Appconfig.REPORTS_API_URL + "get_newsfeed_list/")
                .setBodyParameter("lat", String.valueOf(location.getLatitude()))
                .setBodyParameter("lng", String.valueOf(location.getLongitude()))
                .setBodyParameter("order_by", sort)
                .setBodyParameter("filter", filterStr)
                .setBodyParameter("limit", Appconfig.FETCH_LIMIT)
                .asJsonObject()
                .setCallback(callback);
    }

    public static void getMore(Context context, int offset, FutureCallback<JsonObject> callback,
                               Location location, String sort, ArrayList<Integer> filters) {
        String filterStr = "";
        for (Integer i : filters) {
            filterStr += i + (i != filters.get(filters.size() - 1) ? "," : "");
        }

        Ion.with(context)
                .load(Appconfig.REPORTS_API_URL + "get_newsfeed_list/")
                .setBodyParameter("lat", String.valueOf(location.getLatitude()))
                .setBodyParameter("lng", String.valueOf(location.getLongitude()))
                .setBodyParameter("order_by", sort)
                .setBodyParameter("filter", filterStr)
                .setBodyParameter("limit", Appconfig.FETCH_LIMIT)
                .setBodyParameter("offset", offset + "")
                .asJsonObject()
                .setCallback(callback);
    }

    public static void getLocationReport(Context context, int location_id, FutureCallback<JsonObject> callback) {
        Ion.with(context)
                .load(Appconfig.REPORTS_API_URL + "get_location_report/")
                .setBodyParameter("location_id", location_id+"")
                .setBodyParameter("limit", Appconfig.FETCH_LIMIT)
                .asJsonObject()
                .setCallback(callback);

    }

    public List<NewsfeedImage> getImages() {
        return NewsfeedImage.find(NewsfeedImage.class, "owner_id = ? ", getServerId() + "");
    }

    public int deleteImages(){
        return NewsfeedImage.deleteAll(NewsfeedImage.class, "owner_id = ?", getServerId() + "");
    }
}
