package com.mobilus.contagio.model;

import android.content.Context;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.config.Appconfig;
import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by user on 27/10/2015.
 */
public class Location extends SugarRecord implements Serializable {

    private String serverId;
    private String name;
    private String lat;
    private String lng;
    private String apiId;
    private int apiType;
    private Long createdAt;
    private JsonArray latestUpdates;
    private String coverImage;

    public Location(JsonObject jsonObject){
        fromJson(jsonObject);
    }

    public void fromJson(JsonObject jsonObject) {
        this.serverId = jsonObject.get("id").getAsString();
        this.name = jsonObject.get("name").getAsString();
        this.lat = jsonObject.get("lat").getAsString();
        this.lng = jsonObject.get("lng").getAsString();
        this.apiId = jsonObject.get("api_id").getAsString();
        this.createdAt = jsonObject.get("created_at").getAsLong();
        if(jsonObject.has("latest_updates")) {
            this.latestUpdates = jsonObject.get("latest_updates").getAsJsonArray();
        }
        if(jsonObject.has("cover_image")) {
            this.coverImage = jsonObject.get("cover_image").getAsString();
        }
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public int getApiType() {
        return apiType;
    }

    public void setApiType(int apiType) {
        this.apiType = apiType;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public JsonArray getLatestUpdates() {
        return latestUpdates;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public static void getOne(Context context, int location_id, FutureCallback<JsonObject> callback){
        Ion.with(context)
                .load(Appconfig.LOCATIONS_API_URL + "get_one/")
                .setBodyParameter("id", location_id+"")
                .asJsonObject()
                .setCallback(callback);
    }
}
