package com.mobilus.contagio.model;

import android.content.Context;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.config.Appconfig;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

/**
 * Created by user on 29/10/2015.
 */
public class Event extends SugarRecord implements Serializable {
    private String serverId;
    private String userId;
    private String name;
    private Long dateTime;
    private String organizer;
    private Long createdAt;
    private Long updatedAt;

    private Location location;

    public Event(JsonObject jsonObject, Location location){
        fromJson(jsonObject, location);
    }

    public void fromJson(JsonObject jsonObject, Location location) {
        this.serverId = jsonObject.get("id").getAsString();
        this.userId = jsonObject.get("user_id").getAsString();
        if(jsonObject.has("name")){
            this.name = jsonObject.get("name").getAsString();
        }else{
            this.name = jsonObject.get("event_name").getAsString();
        }
        this.dateTime = jsonObject.get("datetime").getAsLong();
        this.organizer = jsonObject.get("organizer").getAsString();
        this.createdAt = jsonObject.get("created_at").getAsLong();
        this.updatedAt = jsonObject.get("updated_at").getAsLong();
        this.location = location;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDateTime() {
        return dateTime;
    }

    public void setDateTime(Long dateTime) {
        this.dateTime = dateTime;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<EventImage> getImages() {
        return EventImage.find(EventImage.class, "event_id = ? ", getServerId());
    }

    public int deleteImages(){
        return EventImage.deleteAll(EventImage.class, "event_id = ?", getServerId());
    }

    public static void getOne(Context context, String event_id, FutureCallback<JsonObject> callback){
        Ion.with(context)
                .load(Appconfig.EVENTS_API_URL + "get_one/")
                .setBodyParameter("id", event_id+"")
                .asJsonObject()
                .setCallback(callback);
    }
}
