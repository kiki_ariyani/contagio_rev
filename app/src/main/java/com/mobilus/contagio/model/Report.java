package com.mobilus.contagio.model;

import android.content.Context;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.config.Appconfig;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

/**
 * Created by user on 22/10/2015.
 */
public class Report extends SugarRecord implements Serializable {

    private String serverId;
    private String userId;
    private int type;
    private String comment;
    private Long date;
    private Long createdAt;
    private Long updatedAt;
    private int victim;

    private Location location;

    public Report(JsonObject jsonObject, Location location){
        fromJson(jsonObject, location);
    }

    public void fromJson(JsonObject jsonObject, Location location) {
        this.serverId = jsonObject.get("id").getAsString();
        this.userId = jsonObject.get("user_id").getAsString();
        if(jsonObject.has("type")){
            this.type = jsonObject.get("type").getAsInt();
        }else{
            this.type = jsonObject.get("report_type").getAsInt();
        }

        this.comment = jsonObject.get("comment").getAsString();
        this.date = jsonObject.get("date").getAsLong();
        this.createdAt = jsonObject.get("created_at").getAsLong();
        this.updatedAt = jsonObject.get("updated_at").getAsLong();
        this.victim = jsonObject.get("victim").getAsInt();
        this.location = location;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getVictim() {
        return victim;
    }

    public void setVictim(int victim) {
        this.victim = victim;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<ReportImage> getImages() {
        return ReportImage.find(ReportImage.class, "report_id = ? ", getServerId());
    }

    public int deleteImages(){
        return ReportImage.deleteAll(ReportImage.class, "report_id = ?", getServerId());
    }

    public static void getOne(Context context, String report_id, FutureCallback<JsonObject> callback){
        Ion.with(context)
                .load(Appconfig.REPORTS_API_URL + "get_one/")
                .setBodyParameter("id", report_id+"")
                .asJsonObject()
                .setCallback(callback);
    }

    public static void getGraphDataByType(Context context, FutureCallback<JsonObject> callback){
        Ion.with(context)
                .load(Appconfig.REPORTS_API_URL+ "graph_report_by_type/")
                .asJsonObject()
                .setCallback(callback);
    }
}
