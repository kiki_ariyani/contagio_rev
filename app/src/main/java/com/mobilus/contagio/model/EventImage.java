package com.mobilus.contagio.model;

import android.content.Context;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.config.Appconfig;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by user on 29/10/2015.
 */
public class EventImage extends SugarRecord implements Serializable{
    private final String TAG = "EventImage";

    private String serverId;
    private String eventId;
    private String name;
    private Long createdAt;
    private Long updatedAt;
    private String path;

    private Event event;

    public EventImage(){

    }

    public EventImage(JsonObject jsonObject, Event event) {
        this.serverId = jsonObject.get("id").getAsString();
        this.eventId = jsonObject.get("event_id").getAsString();
        this.name = jsonObject.get("name").getAsString();
        this.createdAt = jsonObject.get("created_at").getAsLong();
        this.updatedAt = jsonObject.get("updated_at").getAsLong();
        if(jsonObject.has("path")){
            this.path = jsonObject.get("path").getAsString();
        }
        this.event = event;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public static void getList(Context context, String id, FutureCallback<JsonObject> callback) {
        Ion.with(context)
                .load(Appconfig.EVENTS_API_URL + "get_images/")
                .setBodyParameter("event_id", id)
                .asJsonObject()
                .setCallback(callback);
    }
}
