package com.mobilus.contagio.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.mobilus.contagio.R;
import com.readystatesoftware.systembartint.SystemBarTintManager;

/**
 * Created by root on 10/16/15.
 */
public class WindowCompatUtil {
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarcolor(Window window, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(color);
        }
    }

    public static void initStatusBarColor(Activity activity, View root) {
        final int color = R.color.primary_dark;
        SystemBarTintManager tintManager = new SystemBarTintManager(activity);
        final int statusBarHeight = tintManager.getConfig().getStatusBarHeight();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

            if (root == null) {
                tintManager.setStatusBarTintResource(color);
                tintManager.setStatusBarTintEnabled(true);
                root = activity.findViewById(android.R.id.content);
            }
            root.setPadding(0, statusBarHeight, 0, 0);
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && root != null){
            activity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            root.setPadding(0, statusBarHeight, 0, 0);
        }
    }

    public static void hideSoftKeyboard(Activity activity){
        InputMethodManager inputManager =
                (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
