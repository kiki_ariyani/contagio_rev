package com.mobilus.contagio.helper;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import java.lang.reflect.Field;

public class GsonUtils {
	private static Gson gson;
	private static JsonParser parser;
	
	public static Gson getGson(){
		if(gson == null){
			gson = new GsonBuilder()
            .setFieldNamingStrategy(new MyFieldNamingStrategy())
			.create();
		}
		
		return gson;
	}

	private static class MyFieldNamingStrategy implements FieldNamingStrategy
	{
		private static String separateCamelCase(String name, String separator) {
			StringBuilder translation = new StringBuilder();
			for (int i = 0; i < name.length(); i++) {
				char character = name.charAt(i);
				if (Character.isUpperCase(character) && translation.length() != 0) {
					translation.append(separator);
				}
				translation.append(character);
			}
			return translation.toString();
		}

		@Override
		public String translateName(Field f) {
			String name = f.getName();
			if(name.equalsIgnoreCase("id")){
				return "id_device";
			}

			if(name.equalsIgnoreCase("idServer") || name.equalsIgnoreCase("serverId")){
				return "id";
			}

			return separateCamelCase(name, "_").toLowerCase();
		}
	}

	public static JsonParser getParser(){
		if(parser == null){
			parser = new JsonParser();
		}
		
		return parser;
	}
}
