package com.mobilus.contagio.helper;

import android.app.Activity;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.github.clans.fab.FloatingActionMenu;
import com.mobilus.contagio.R;

/**
 * Created by user on 27/11/2015.
 */
public class ShowCaseViewHelper implements OnShowcaseEventListener {
    ShowcaseView sv;
    Activity mActivity;
    ViewTarget mNextTarget;
    String mNextTitle;
    String mNextContent;
    boolean mNextButton = false;
    String mNextBtnAlign;
    RelativeLayout.LayoutParams layoutParams;

    public void show(Activity activity,ViewTarget target, String title, String content,String btnAlign, ViewTarget nextTarget, String nextTitle, String nextContent,String nextBtnAlign,boolean nextBtn){
        show(activity,target, title, content, true, btnAlign);
        this.mActivity = activity;
        this.mNextTarget = nextTarget;
        this.mNextTitle = nextTitle;
        this.mNextContent = nextContent;
        this.mNextButton = nextBtn;
        this.mNextBtnAlign = nextBtnAlign;
    }

    public void showToolbarAsTarget(Activity activity,ToolbarActionItemTarget target, String title, String content, boolean nextBtn, String btnAlign){
        sv = new ShowcaseView.Builder(activity)
                .setTarget(target)
                .setContentTitle(title)
                .setContentText(content)
                .setStyle(R.style.CustomShowcaseTheme2)
                .build();

        if(btnAlign != null){
            if(btnAlign.equals("LeftButtom")){
                layoutParams = buttonToLeftBottom();
            }else if(btnAlign.equals("CenterRight")){
                layoutParams = buttonToCenterRight();
            }
            sv.setButtonPosition(layoutParams);
        }

        if(nextBtn){
            sv.setOnShowcaseEventListener(this);
        }

        sv.show();
    }

    public void show(Activity activity,ViewTarget target, String title, String content, boolean nextBtn, String btnAlign){
        if(target != null){
            sv = new ShowcaseView.Builder(activity)
                    .setTarget(target)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .build();
        }else{
            sv = new ShowcaseView.Builder(activity)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .build();
        }

        if(btnAlign != null){
            if(btnAlign.equals("LeftButtom")){
                layoutParams = buttonToLeftBottom();
            }else if(btnAlign.equals("CenterRight")){
                layoutParams = buttonToCenterRight();
            }
            sv.setButtonPosition(layoutParams);
        }

        if(nextBtn){
            sv.setOnShowcaseEventListener(this);
        }

        sv.show();
    }

    public RelativeLayout.LayoutParams buttonToLeftBottom(){
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        layoutParams.setMargins(20, 20, 20, 20);

        return layoutParams;
    }

    public RelativeLayout.LayoutParams buttonToCenterRight(){
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        layoutParams.setMargins(20,20,20,20);

        return layoutParams;
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
        if(mNextButton){
            show(this.mActivity,this.mNextTarget, this.mNextTitle, this.mNextContent, false, this.mNextBtnAlign);
        }
    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }
}
