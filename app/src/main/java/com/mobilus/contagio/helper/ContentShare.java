package com.mobilus.contagio.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

/**
 * Created by user on 18/11/2015.
 */
public class ContentShare{

    public void share(Activity activity, String subject, String content){
        String fullContent = content+"\n\nShared from Contageo app - http://contageo.co";
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(Intent.EXTRA_TEXT, fullContent);
        activity.startActivity(Intent.createChooser(shareIntent, "Share via"));
    }
}
