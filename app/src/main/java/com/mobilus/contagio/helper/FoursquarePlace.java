package com.mobilus.contagio.helper;

import android.location.Location;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by user on 26/10/2015.
 */
public class FoursquarePlace {
    private static final String TAG = FoursquarePlace.class.getSimpleName();

    private static final String FOURSQUARE_API_BASE = "https://api.foursquare.com/v2/";
    private static final String TYPE_AUTOCOMPLETE = "venues/suggestcompletion";
    private static final String FOURSQUARE_CLIENT_ID = "UAFKI1XYJYXLROZHTL1FJK3OFVIDDRSEFTOYZMJJ2NFUXKOD";
    private static final String FOURSQUARE_CLIENT_SECRET = "WGCRU2LVSYQIPVKES3CNB4ATO2GHDRW1WNTZZSLGERKK4TUU";

    public JSONObject autocomplete (String input, Location location) {
        JSONObject resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        String ll = (location != null ? String.valueOf(location.getLatitude())+','+String.valueOf(location.getLongitude()) : "-6.899193,107.619111");
        Log.i("LatLong",ll);
        try {
            StringBuilder sb = new StringBuilder(FOURSQUARE_API_BASE + TYPE_AUTOCOMPLETE);
            sb.append("?ll=" + ll);
            sb.append("&client_id="+ FOURSQUARE_CLIENT_ID);
            sb.append("&query=" + URLEncoder.encode(input, "utf8"));
            sb.append("&client_secret=" +FOURSQUARE_CLIENT_SECRET);
            sb.append("&v=20151026");

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            Log.d(TAG, jsonResults.toString());

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            if(jsonObj.getJSONObject("response").getJSONArray("minivenues").length() > 0){
                resultList = jsonObj.getJSONObject("response");
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }
}
