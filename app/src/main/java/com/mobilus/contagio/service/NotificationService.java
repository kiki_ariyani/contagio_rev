package com.mobilus.contagio.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.activity.EventDetailActivity;
import com.mobilus.contagio.activity.ReportDetailActivity;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.model.Event;
import com.mobilus.contagio.model.Report;
import com.mobilus.contagio.util.LocationUtil;

public class NotificationService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String TAG = "NotificationService";
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private boolean mRequestingLocationUpdates = true;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        buildGoogleApiClient();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mCurrentLocation = mLastLocation;

        if(mCurrentLocation !=  null){
            Log.d(TAG, "current location: "+ mCurrentLocation);
            createLocationRequest();
            requestContentNearby();

            if (mRequestingLocationUpdates) {
                startLocationUpdates();
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Contageo Notification service connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Contageo Notification service connection failed");
    }

    public void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        Float distance = mLastLocation.distanceTo(mCurrentLocation); // in meters

        if(distance >= 3000){ // instead 3km
            requestContentNearby();
        }
    }

    public void requestContentNearby(){
        Ion.with(this)
                .load(Appconfig.NOTIFICATIONS_API_URL + "get/")
                .setBodyParameter("lat", String.valueOf(mCurrentLocation.getLatitude()))
                .setBodyParameter("lng", String.valueOf(mCurrentLocation.getLongitude()))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.d(TAG, "service result: " + result);
                        if (result != null){
                            if (result.get("status").getAsInt() == 1) {
                                JsonObject data = result.get("data").getAsJsonObject();
                                int type = data.get("content_type").getAsInt();
                                String title;
                                String content;
                                String distance;

                                if(type == Appconfig.NEWSFEED_EVENT){
                                    title = data.get("name").getAsString();
                                    content = data.get("organizer").getAsString();
                                }else{
                                    switch (data.get("type").getAsInt()){
                                        case Appconfig.BREADING_REPORT :
                                            title = getString(R.string.breading_nearby);
                                            break;
                                        case Appconfig.FOGGING_REPORT :
                                            title = getString(R.string.fogging_nearby);
                                            break;
                                     /*   case Appconfig.BITTEN_REPORT :
                                            title = getString(R.string.bitten_nearby);
                                            break;*/
                                        default:
                                            title = getString(R.string.case_nearby);
                                    }
                                    content = data.get("comment").getAsString();
                                }

                                if(data.get("distance").getAsFloat() < 1){
                                    distance = Math.round(data.get("distance").getAsFloat() * 1000) + " m";
                                }else{
                                    distance = Math.round(data.get("distance").getAsFloat()) + " km";
                                }

                                createNotification(title + " (" + distance + ")", content, type, data);
                                mLastLocation = mCurrentLocation;
                            }
                        }
                    }
                });
    }

    public void createNotification(String title, String content, int type, JsonObject data){
        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        // Init intent to Content Detail Activity
        Intent notificationIntent;
        com.mobilus.contagio.model.Location location =
                new com.mobilus.contagio.model.Location(data.get("location").getAsJsonObject());
        if(type == Appconfig.NEWSFEED_REPORT){
            Report report = new Report(data, location);
            notificationIntent = new Intent(this, ReportDetailActivity.class);
            notificationIntent.putExtra("report", new Gson().toJson(report));
        }else{
            Event event = new Event(data, location);
            notificationIntent = new Intent(this, EventDetailActivity.class);
            notificationIntent.putExtra("event", new Gson().toJson(event));
        }

        PendingIntent intent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setAutoCancel(true)
                        .setContentIntent(intent);

        mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);

        mBuilder.setOngoing(false);

        Notification notification = mBuilder.build();
        notificationManager.notify(0, notification);
    }
}
