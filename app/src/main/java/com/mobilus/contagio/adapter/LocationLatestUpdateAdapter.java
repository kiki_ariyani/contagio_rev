package com.mobilus.contagio.adapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mobilus.contagio.R;
import com.mobilus.contagio.activity.EventDetailActivity;
import com.mobilus.contagio.activity.ReportDetailActivity;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.model.Event;
import com.mobilus.contagio.model.Location;
import com.mobilus.contagio.model.Report;
import com.mobilus.contagio.model.ReportImage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by user on 03/11/2015.
 */
public class LocationLatestUpdateAdapter extends RecyclerView.Adapter<LocationLatestUpdateAdapter.ViewHolder> {
    private JsonArray itemData;
    private Context context;
    private Location location;

    public LocationLatestUpdateAdapter(JsonArray data,Context context, Location location){
        this.itemData = data;
        this.context= context;
        this.location = location;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_latest_update, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JsonElement item = itemData.get(position);
        JsonObject parsedData = item.getAsJsonObject();

        int report_type = parsedData.get("report_type").getAsInt();
        if(report_type >= 0){
            holder.itemTitle.setText(Appconfig.REPORT_TYPES[report_type]);
        }else{
            holder.itemTitle.setText(parsedData.get("event_name").getAsString());
        }

        holder.itemTime.setText(DateUtils.getRelativeTimeSpanString(parsedData.get("created_at").getAsLong(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        holder.itemCreator.setText("Submitted by " + parsedData.get("creator").getAsString());
        if(report_type == Appconfig.CASE_REPORT){
            holder.itemIcon.setImageResource(R.drawable.ic_mini_report);
        }else if(report_type == Appconfig.BREADING_REPORT){
            holder.itemIcon.setImageResource(R.drawable.ic_mini_breeding);
        }else if(report_type == Appconfig.FOGGING_REPORT){
            holder.itemIcon.setImageResource(R.drawable.ic_mini_fogging);
        }/*else if(report_type == Appconfig.BITTEN_REPORT){
            holder.itemIcon.setImageResource(R.drawable.ic_mini_bitten);
        }*/else{
            holder.itemIcon.setImageResource(R.drawable.ic_mini_event);
        }

        holder.itemRow.setOnClickListener(getItemClickListener(parsedData));
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView itemTitle;
        TextView itemTime;
        TextView itemCreator;
        ImageView itemIcon;
        ViewGroup itemRow;

        public ViewHolder(View itemView) {
            super(itemView);
            itemTitle = (TextView) itemView.findViewById(R.id.item_title);
            itemTime = (TextView) itemView.findViewById(R.id.item_time);
            itemCreator = (TextView) itemView.findViewById(R.id.item_creator);
            itemIcon = (ImageView) itemView.findViewById(R.id.item_icon);
            itemRow = (ViewGroup) itemView.findViewById(R.id.item_row);
        }
    }

    protected View.OnClickListener getItemClickListener(final JsonObject item){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.get("report_type").getAsInt() < 0){
                    //event intent
                    final Intent i = new Intent(context, EventDetailActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Event event = new Event(item, location);
                    event.save();
                    i.putExtra("event", new Gson().toJson(event));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            context.startActivity(i);
                        }
                    }, 300);
                }else{
                    //report intent
                    Report report = new Report(item, location);
                    report.save();

                    report.deleteImages();
                    JsonArray reportImages = item.getAsJsonArray("images");
                    for (int j=0;j<reportImages.size();j++){
                        ReportImage reportImage = new ReportImage(reportImages.get(j).getAsJsonObject(),report);
                        reportImage.save();
                    }

                    final Intent i = new Intent(context, ReportDetailActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("report", new Gson().toJson(report));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            context.startActivity(i);
                        }
                    }, 300);
                }
            }
        };
    }
}
