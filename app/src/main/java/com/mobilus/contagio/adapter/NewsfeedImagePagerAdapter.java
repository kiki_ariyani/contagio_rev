package com.mobilus.contagio.adapter;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.activity.ViewImagesActivity;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.model.Newsfeed;
import com.mobilus.contagio.model.NewsfeedImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desi on 29 Oct 02015.
 */
public class NewsfeedImagePagerAdapter extends PagerAdapter{
    private static final String TAG = "NewsfeedImagePagerAdapter";
    private List<NewsfeedImage> mNewsfeedImageList;
    private Newsfeed mNewsfeed;
    private ArrayList<JSONObject> imgs = new ArrayList<>();

    public NewsfeedImagePagerAdapter(List<NewsfeedImage> images, Newsfeed newsfeed) {
        this.mNewsfeedImageList = images;
        this.mNewsfeed = newsfeed;

        for ( NewsfeedImage newsfeedImg: mNewsfeedImageList) {
            JSONObject img = new JSONObject();
            try {
                img.put("id", newsfeedImg.getServerId());
                img.put("name", newsfeedImg.getName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            imgs.add(img);
        }
    }

    public int getCount() {
        return mNewsfeedImageList.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public NewsfeedImage getItem(int position) {
        return mNewsfeedImageList.get(position);
    }

    public View instantiateItem(ViewGroup container, final int position) {
        ImageView imgView = new ImageView(container.getContext());
        final NewsfeedImage item = getItem(position);

        if(mNewsfeed.getType() == Appconfig.NEWSFEED_REPORT){
            Ion.with(imgView).load(Appconfig.REPORT_IMAGE_ASSET_URL + item.getName());
        }else if(mNewsfeed.getType() == Appconfig.NEWSFEED_EVENT){
            Ion.with(imgView).load(Appconfig.EVENT_IMAGE_ASSET_URL + item.getName());
        }else{
            Ion.with(imgView).load(Appconfig.REPORT_IMAGE_ASSET_URL + item.getName());
        }
        imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ViewImagesActivity.class);
                i.putExtra("images", imgs.toString());
                i.putExtra("position", position);
                i.putExtra("itemType", mNewsfeed.getType());
                v.getContext().startActivity(i);
            }
        });

        container.addView(imgView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return imgView;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
