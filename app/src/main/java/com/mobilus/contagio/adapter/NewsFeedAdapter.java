package com.mobilus.contagio.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.mobilus.contagio.R;
import com.mobilus.contagio.activity.EventActivity;
import com.mobilus.contagio.activity.EventDetailActivity;
import com.mobilus.contagio.activity.MapViewActivity;
import com.mobilus.contagio.activity.NewsfeedActivity;
import com.mobilus.contagio.activity.NewsfeedDetailActivity;
import com.mobilus.contagio.activity.ReportActivity;
import com.mobilus.contagio.activity.ReportDetailActivity;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.model.Event;
import com.mobilus.contagio.model.EventImage;
import com.mobilus.contagio.model.Location;
import com.mobilus.contagio.model.Newsfeed;
import com.mobilus.contagio.model.Report;
import com.mobilus.contagio.model.ReportImage;

import java.util.List;

/**
 * Created by root on 10/19/15.
 */
public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.ViewHolder>{
    private static final String TAG = "NewsfeedAdapter";
    private List<Newsfeed> newsFeedData;
    private Context context;

    public NewsFeedAdapter(List<Newsfeed> newsfeed) {
        this.newsFeedData = newsfeed;
    }

    @Override
    public int getItemCount() {
        return newsFeedData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        View locationContent;
        View reportContent;
        View eventContent;
        ImageView img;
        TextView location;
        TextView distance;

        // location views
        View caseContent;
        View noCaseContent;
        TextView caseNum;
        TextView victims;
        TextView lastCase;

        // report views
        TextView reportTime;
        TextView reportMapLink;

        // event views
        TextView comment;
        TextView eventMapLink;

        ViewGroup content;

        public ViewHolder(View view) {
            super(view);
            this.locationContent = view.findViewById(R.id.location_content);
            this.reportContent = view.findViewById(R.id.report_content);
            this.eventContent = view.findViewById(R.id.event_content);
            this.img = (ImageView) view.findViewById(R.id.img);
            this.location = (TextView) view.findViewById(R.id.location);
            this.distance = (TextView) view.findViewById(R.id.distance);
            this.caseContent = view.findViewById(R.id.there_is_case);
            this.noCaseContent = view.findViewById(R.id.no_case);
            this.caseNum = (TextView) view.findViewById(R.id.case_num);
            this.victims = (TextView) view.findViewById(R.id.victims);
            this.lastCase = (TextView) view.findViewById(R.id.last_case);
            this.reportTime = (TextView) view.findViewById(R.id.report_time);
            this.reportMapLink = (TextView) view.findViewById(R.id.view_report_map);
            this.comment = (TextView) view.findViewById(R.id.comment);
            this.eventMapLink = (TextView) view.findViewById(R.id.view_event_map);
            this.content = (ViewGroup) view.findViewById(R.id.row_content);
        }
    }

    @Override
    public NewsFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_newsfeed, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Newsfeed item = newsFeedData.get(position);

        if(item.getType() == Appconfig.NEWSFEED_LOCATION){ // fill location content
            holder.reportContent.setVisibility(View.GONE);
            holder.eventContent.setVisibility(View.GONE);
            holder.locationContent.setVisibility(View.VISIBLE);
            holder.img.setImageResource(R.drawable.pin);
            holder.location.setText(item.getAddress());

            if(item.getCaseNumWeek() > 0){
                holder.caseContent.setVisibility(View.VISIBLE);
                holder.noCaseContent.setVisibility(View.GONE);
                holder.caseNum.setText(item.getCaseNumWeek()+"");
                holder.victims.setText(item.getVictims()+"");
            }else{
                holder.caseContent.setVisibility(View.GONE);
                holder.noCaseContent.setVisibility(View.VISIBLE);
                holder.lastCase.setText(DateUtils.getRelativeTimeSpanString(item.getLastCaseDate()));
            }
        }else if (item.getType() == Appconfig.NEWSFEED_REPORT){ // fill report content
            holder.reportContent.setVisibility(View.VISIBLE);
            holder.eventContent.setVisibility(View.GONE);
            holder.locationContent.setVisibility(View.GONE);

            if (item.getReportType() == Appconfig.BREADING_REPORT){
                holder.img.setImageResource(R.drawable.breeding);
                holder.location.setText(R.string.breading_nearby);
            } else if (item.getReportType() == Appconfig.FOGGING_REPORT){
                holder.img.setImageResource(R.drawable.fogging);
                holder.location.setText(R.string.fogging_nearby);
            }else{
                holder.img.setImageResource(R.drawable.report);
                holder.location.setText(R.string.case_nearby);
            }/* else {
                holder.img.setImageResource(R.drawable.bitten);
                holder.location.setText(R.string.bitten_nearby);
            }*/
            holder.reportTime.setText(DateUtils.getRelativeTimeSpanString(item.getCreatedAt()));
            holder.reportMapLink.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            holder.reportMapLink.setOnClickListener(goToMap(item));
        }else{ // fill event content
            holder.reportContent.setVisibility(View.GONE);
            holder.eventContent.setVisibility(View.VISIBLE);
            holder.locationContent.setVisibility(View.GONE);

            holder.img.setImageResource(R.drawable.owner);
            holder.location.setText(item.getEventCreator() + " has updated an event: " + item.getEventName());
            holder.comment.setText(item.getOrganizer());
            holder.eventMapLink.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            holder.eventMapLink.setOnClickListener(goToMap(item));
        }

        if(item.getDistance() < 1){
            holder.distance.setText(Math.round(item.getDistance() * 1000) + " m");
        }else{
            holder.distance.setText(Math.round(item.getDistance()) + " km");
        }

        holder.content.setOnClickListener(getItemClickListener(item));
    }

    private View.OnClickListener goToMap(final Newsfeed item) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent i = new Intent(context, MapViewActivity.class);
                i.putExtra("newsfeed_id", item.getServerId());
                i.putExtra("newsfeed_type", item.getType());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        context.startActivity(i);
                    }
                }, 300);
            }
        };
    }

    protected View.OnClickListener getItemClickListener(final Newsfeed item){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.getType() == Appconfig.NEWSFEED_REPORT){
                    Log.d("GET_REPORT","id : "+item.getServerId());
                    Report.getOne(context, String.valueOf(item.getServerId()), new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Log.d("GET_REPORT_E", e.getMessage());
                                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                            } else {
                                if (result.get("status").getAsInt() == 1) {
                                    Location dataLocation = new Location(result.getAsJsonObject("data").getAsJsonObject("location"));
                                    dataLocation.save();
                                    Report dataReport = new Report(result.getAsJsonObject("data"), dataLocation);
                                    dataReport.save();

                                    dataReport.deleteImages();
                                    JsonArray reportImages = result.getAsJsonObject("data").getAsJsonArray("images");
                                    for (int j = 0; j < reportImages.size(); j++) {
                                        ReportImage reportImage = new ReportImage(reportImages.get(j).getAsJsonObject(), dataReport);
                                        reportImage.save();
                                    }

                                    final Intent i = new Intent(context, ReportDetailActivity.class);
                                    i.putExtra("report", new Gson().toJson(dataReport));
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            context.startActivity(i);
                                        }
                                    }, 300);
                                    Log.d("GET_REPORT", "result" + result);
                                } else {
                                    Toast.makeText(context, result.get("msg").getAsString(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });
                }else if(item.getType() == Appconfig.NEWSFEED_EVENT){
                    Log.d("GET_EVENT","id : "+item.getServerId());
                    Event.getOne(context, String.valueOf(item.getServerId()), new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Log.d("GET_EVENT_E", e.getMessage());
                                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                            } else {
                                if (result.get("status").getAsInt() == 1) {
                                    Location dataLocation = new Location(result.getAsJsonObject("data").getAsJsonObject("location"));
                                    dataLocation.save();
                                    Event dataEvent = new Event(result.getAsJsonObject("data"), dataLocation);
                                    dataEvent.save();

                                    dataEvent.deleteImages();
                                    JsonArray eventImages = result.getAsJsonObject("data").getAsJsonArray("images");
                                    for (int j = 0; j < eventImages.size(); j++) {
                                        EventImage eventImage = new EventImage(eventImages.get(j).getAsJsonObject(), dataEvent);
                                        eventImage.save();
                                    }

                                    final Intent i = new Intent(context, EventDetailActivity.class);
                                    i.putExtra("event", new Gson().toJson(dataEvent));
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            context.startActivity(i);
                                        }
                                    }, 300);
                                    Log.d("GET_EVENT", "result" + result);
                                } else {
                                    Toast.makeText(context, result.get("msg").getAsString(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });
                }else if(item.getType() == Appconfig.NEWSFEED_LOCATION){
                    final Intent i = new Intent(context, NewsfeedDetailActivity.class);
                    i.putExtra("newsfeed", new Gson().toJson(item));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            context.startActivity(i);
                        }
                    }, 300);
                }

            }
        };
    }

}
