package com.mobilus.contagio.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.koushikdutta.ion.Ion;
import com.mobilus.contagio.R;
import com.mobilus.contagio.config.Appconfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by user on 21/10/2015.
 */
public class ChooseImageAdapter extends ArrayAdapter<JSONObject> {
    JSONArray deletedImg = new JSONArray();

    private boolean hideDeleteBtn;
    private String imgPathUrl;

    private OnItemEmpty onItemEmpty = null;

    public ChooseImageAdapter(Context context, ArrayList<JSONObject> imagesArray, boolean viewMode, String imgPathUrl) {
        super(context, R.layout.item_image, imagesArray);
        this.hideDeleteBtn = viewMode;
        this.imgPathUrl = imgPathUrl;
    }

    public void setOnItemEmpty(OnItemEmpty onItemEmpty){
        this.onItemEmpty = onItemEmpty;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // get layout from mobile.xml
        View gridView = inflater.inflate(R.layout.item_image, null);

        // set image based on selected text
        ImageView imageView = (ImageView) gridView
                .findViewById(R.id.grid_item_image);

        ImageButton imageButton = (ImageButton) gridView
                .findViewById(R.id.delete_item_image);
        imageButton.setTag(position + "");

        final JSONObject img = getItem(position);

        imageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog diaBox = AskOption();
                diaBox.show();
                diaBox.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View dlgview) {
                                final int id = img.optInt("id");
                                if (id > 0) {
                                    deletedImg.put(id);
                                }
                                remove(img);
                                notifyDataSetChanged();
                                diaBox.dismiss();
                                if(isEmpty() && onItemEmpty != null){
                                    onItemEmpty.onItemEmpty();
                                }
                            }
                        });
            }
        });

        final int imageId = img.optInt("id");
        String imgUrl = imgPathUrl + img.optString("name");

        if (imageId <= 0) {
            imgUrl = img.optString("path");
        }

        Ion.with(imageView).load(imgUrl);
        imageView.setAdjustViewBounds(true);
        PhotoViewAttacher mAttacher = new PhotoViewAttacher(imageView);
        mAttacher.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mAttacher.setZoomable(false);

        try {
            img.put("position", position + " ");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(hideDeleteBtn){
            imageButton.setVisibility(View.GONE);
        }
        gridView.setTag(img);

        return gridView;
    }

    public JSONArray getDeletedImg() {
        return deletedImg;
    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getContext())
                // set message, title, and icon
                .setMessage("Delete Image?")

                .setPositiveButton("Delete",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                // your deleting code
                                dialog.dismiss();
                            }

                        })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                dialog.dismiss();

                            }
                        }).create();
        return myQuittingDialogBox;

    }

    public static interface OnItemEmpty{
        public void onItemEmpty();
    }
}
