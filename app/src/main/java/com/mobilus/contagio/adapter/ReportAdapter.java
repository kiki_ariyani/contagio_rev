package com.mobilus.contagio.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mobilus.contagio.R;
import com.mobilus.contagio.activity.ReportDetailActivity;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.model.Location;
import com.mobilus.contagio.model.Report;

import java.util.List;

/**
 * Created by Desi on 30 Oct 02015.
 */
public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder>{
    private static final String TAG = "ReportAdapter";
    private List<Report> reportData;
    private Location location;
    private Context context;

    public ReportAdapter(List<Report> reportList, Location location) {
        this.reportData = reportList;
        this.location = location;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView title;
        TextView timeElapsed;
        TextView creator;
        ViewGroup content;

        public ViewHolder(View view) {
            super(view);
            this.icon = (ImageView) view.findViewById(R.id.icon);
            this.title = (TextView) view.findViewById(R.id.title);
            this.timeElapsed = (TextView) view.findViewById(R.id.time_elapsed);
            this.creator = (TextView) view.findViewById(R.id.creator);
            this.content = (ViewGroup) view.findViewById(R.id.row_content);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Report item = reportData.get(position);

        if (item.getType() == Appconfig.BREADING_REPORT){
            holder.icon.setImageResource(R.drawable.breeding);
            holder.title.setText("Breading Grounds");
        } else if (item.getType() == Appconfig.FOGGING_REPORT){
            holder.icon.setImageResource(R.drawable.fogging);
            holder.title.setText("Fogging");
        }/* else if (item.getType() == Appconfig.BITTEN_REPORT){
            holder.icon.setImageResource(R.drawable.bitten);
            holder.title.setText("Bitten");
        }*/else{
            holder.icon.setImageResource(R.drawable.report);
            holder.title.setText("Cases Report");
        }
        holder.timeElapsed.setText(DateUtils.getRelativeTimeSpanString(item.getCreatedAt()));
        holder.content.setOnClickListener(getItemClickListener(item));
    }

    @Override
    public int getItemCount() {
        return reportData.size();
    }

    protected View.OnClickListener getItemClickListener(final Report item){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent i = new Intent(context, ReportDetailActivity.class);
                i.putExtra("report", new Gson().toJson(item));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        context.startActivity(i);
                    }
                }, 300);
            }
        };
    }
}
