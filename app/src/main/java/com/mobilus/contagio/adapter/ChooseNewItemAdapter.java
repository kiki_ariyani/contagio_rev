package com.mobilus.contagio.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobilus.contagio.R;

/**
 * Created by user on 19/10/2015.
 */
public class ChooseNewItemAdapter extends ArrayAdapter<String> {
    private final LayoutInflater mInflater;
    private String text[] = {"New Report", "New Event"};
    private final int imageResource[] = {R.drawable.ic_paper, R.drawable.ic_calendar};

    public ChooseNewItemAdapter(Context context) {
        super(context,android.R.layout.simple_list_item_2);
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<text.length;i++){
            add(text[i]);
        }
    }

    public void setItems(String[] data){
        clear();
        this.text = data;
        for(int i=0;i<text.length;i++){
            add(data[i]);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        view = mInflater.inflate(R.layout.list_item_choose_new_item, parent, false);
        ((ImageView) view.findViewById(R.id.icon)).setImageResource(imageResource[position]);
        ((TextView) view.findViewById(R.id.text)).setText(text[position]);

        return view;
    }
}
