package com.mobilus.contagio.adapter;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.helper.FoursquarePlace;
import com.mobilus.contagio.helper.PlaceAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 21/10/2015.
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable{
    ArrayList<String> resultList;
    ArrayList<String> resultData;
    JSONObject resultObj;
    int resultSource = Appconfig.SOURCE_FOURSQUARE;

    Context mContext;
    int mResource;
    Location location;
    PlaceAPI mPlaceAPI = new PlaceAPI();

    //PlaceAPI mPlaceAPI = new PlaceAPI();
    FoursquarePlace fsqAPI = new FoursquarePlace();
    public PlacesAutoCompleteAdapter(Context context, int resource, Location location) {
        super(context, resource);
        this.location = location;
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

    public String getResultData(int position){
        return resultData.get(position);
    }

    public int getResultSource(){
        return resultSource;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    if(constraint.toString().length() > 2){
                        resultObj = fsqAPI.autocomplete(constraint.toString(), location);
                        JSONArray predsJsonArray = null;

                        if(resultObj == null){
                            resultSource = Appconfig.SOURCE_GOOGLE;

                            resultObj = mPlaceAPI.autocomplete(constraint.toString());
                            try {
                                predsJsonArray = resultObj.getJSONArray("predictions");
                                // Extract the Place descriptions from the results
                                resultList = new ArrayList<String>(predsJsonArray.length());
                                resultData = new ArrayList<String>(predsJsonArray.length());

                                for (int i = 0; i < predsJsonArray.length(); i++) {
                                    resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                                    resultData.add(predsJsonArray.getJSONObject(i).toString());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            resultSource = Appconfig.SOURCE_FOURSQUARE;
                            try {
                                predsJsonArray = resultObj.getJSONArray("minivenues");
                                // Extract the Place descriptions from the results
                                resultList = new ArrayList<String>(predsJsonArray.length());
                                resultData = new ArrayList<String>(predsJsonArray.length());

                                for (int i = 0; i < predsJsonArray.length(); i++) {
                                    resultList.add(predsJsonArray.getJSONObject(i).getString("name"));
                                    resultData.add(predsJsonArray.getJSONObject(i).toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }
}
