package com.mobilus.contagio.config;

import android.content.Context;

import com.mobilus.contagio.database.ISugarDb;
import com.mobilus.contagio.model.User;
import com.orm.SugarRecord;

/**
 * Created by root on 10/16/15.
 */
public class Appconfig {
 //     public static final String APP_URL = "http://192.168.1.13/contagio_web_rev/";
 //   public static final String APP_URL = "http://192.168.1.2/contagio/";
//    public static final String APP_URL = "http://mobilus-interactive.com/contagio/";
 //   public static final String APP_URL = "http://110.4.42.13/~contageo/contageo/";
    public static final String APP_URL = "http://188.166.228.73/contagio_web_rev/";

    public static final String API_URL = APP_URL + "api/"; // API URL
    public static final String REPORTS_API_URL = API_URL + "reports/";
    public static final String LOCATIONS_API_URL = API_URL + "locations/";
    public static final String EVENTS_API_URL = API_URL + "events/";
    public static final String USERS_API_URL = API_URL + "users/";
    public static final String NOTIFICATIONS_API_URL = API_URL + "notifications/";

    public static final String FETCH_LIMIT = "15";

    public static final String REPORT_IMAGE_ASSET_URL = APP_URL + "assets/attachment/report/";
    public static final String EVENT_IMAGE_ASSET_URL = APP_URL + "assets/attachment/event/";
    public static final String SERVER_KEY = "AIzaSyBkO3muR44n_3Bfn0c_pJnLw6zCWBKDt_c";

    public static final int SOURCE_FOURSQUARE = 1;
    public static final int SOURCE_GOOGLE = 2;

    public static final int NEWSFEED_LOCATION = 1;
    public static final int NEWSFEED_REPORT = 2;
    public static final int NEWSFEED_EVENT = 3;

    public static final int CASE_REPORT = 0;
    public static final int BREADING_REPORT = 1;
    public static final int FOGGING_REPORT = 2;
    //public static final int BITTEN_REPORT = 3;

    public static final String[] REPORT_TYPES = {"Case Report", "Breeding Grounds", "Fogging"};

    public static void resetDB(Context context) {
        /** Reset Database **/
        ISugarDb iSugarDb = new ISugarDb(context);
        iSugarDb.deleteAll();

        SugarRecord.executeQuery("UPDATE sqlite_sequence SET seq = 0");
//        SugarRecord.deleteAll(User.class);
//        SugarRecord.executeQuery("UPDATE sqlite_sequence SET seq = 0");
    }
}
