package com.mobilus.contagio.config;

import com.mobilus.contagio.model.User;

import java.util.List;

/**
 * Created by root on 10/16/15.
 */
public class Credential {
    public static final String AUTH = "Authorization";
    public static final String AUTH_TYPE = "Basic ";
    public static User USER = null;
    private static String AUTH_TOKEN = null;

    public static User getUser(){
        if(USER != null){
            return USER;
        }
        List<User> users = User.find(User.class, null, null, null, null, "1");
        USER = !users.isEmpty() ? users.get(0) : null;

        return USER;
    }

    public static String getAuthToken(){
        getUser();
        if(AUTH_TOKEN == null && USER != null){
            AUTH_TOKEN = USER.getAccessToken();
        }

        return AUTH_TOKEN;
    }

    public static void reset(){
        USER = null;
        AUTH_TOKEN = null;
    }
}
