package com.mobilus.contagio.authentication;

import android.util.Log;

import com.google.gson.Gson;
import com.mobilus.contagio.config.Appconfig;
import com.mobilus.contagio.helper.GsonUtils;
import com.mobilus.contagio.model.User;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 10/16/15.
 */
public class ContagioServerAuthenticate implements ServerAuthenticate {
    private final String TAG = "ServerAuthenticate";

    @Override
    public String userSignUp(String email, String name, String pass, String phone) throws Exception {

        String url = Appconfig.API_URL + "users/register/";

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("email", email));
        nameValuePairs.add(new BasicNameValuePair("password", pass));
        nameValuePairs.add(new BasicNameValuePair("phone", phone));
        nameValuePairs.add(new BasicNameValuePair("name", name));

        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        String authtoken = null;
        try {
            HttpResponse response = httpClient.execute(httpPost);

            String responseString = EntityUtils.toString(response.getEntity());
            Log.i(TAG, "> response = " + responseString);

            if (response.getStatusLine().getStatusCode() != 200) {
                ParseComError error = new Gson().fromJson(responseString, ParseComError.class);
                throw new Exception("Error signing-up ["+error.code+"] - " + error.error);
            }

            JSONObject responseJson = new JSONObject(responseString);
            Log.d(TAG, responseString);

            if (responseJson.getInt("status") == 1) {
                JSONObject data = responseJson.getJSONObject("data");
                authtoken = data.optString("auth_token");
                Log.d(TAG, "json object string user: " + data);

                User loggedUser = GsonUtils.getGson().fromJson(data.getString("user"), User.class);
                Log.d(TAG, "loggedUser: " + loggedUser);
                loggedUser.save();
            } else {
                authtoken = "#" + responseJson.getString("msg");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return authtoken;
    }

    @Override
    public String userSignIn(String user, String pass, String authType, String lastToken) throws Exception {

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = Appconfig.API_URL + "users/login/";

        HttpPost httpPost = new HttpPost(url);

        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
        nameValuePairs.add(new BasicNameValuePair("email", user));
        nameValuePairs.add(new BasicNameValuePair("password", pass));
        nameValuePairs.add(new BasicNameValuePair("lastToken", lastToken));
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        String authtoken = null;
        try {
            HttpResponse response = httpClient.execute(httpPost);

            String responseString = EntityUtils.toString(response.getEntity());
            Log.i(TAG, "> response = " + responseString);

            if (response.getStatusLine().getStatusCode() != 200) {
                ParseComError error = new Gson().fromJson(responseString, ParseComError.class);
                throw new Exception("Error signing-in ["+error.code+"] - " + error.error);
            }

            JSONObject responseJson = new JSONObject(responseString);

            if (responseJson.getInt("status") == 1) {
                JSONObject data = responseJson.getJSONObject("data");
                authtoken = data.optString("auth_token");

                User loggedUser = GsonUtils.getGson().fromJson(data.getString("user"), User.class);
                loggedUser.save();
            } else {
                authtoken = "#" + responseJson.getString("msg");
            }
        } catch (IOException e) {
            Log.i(TAG, "> ");
            e.printStackTrace();

        }

        return authtoken;
    }


    private class ParseComError implements Serializable {
        int code;
        String error;
    }
}
