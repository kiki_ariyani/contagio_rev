package com.mobilus.contagio.authentication;

/**
 * Created by root on 10/16/15.
 */
public interface ServerAuthenticate {
    public String userSignUp(final String email, final String name, final String pass, String phone) throws Exception;
    public String userSignIn(final String user, final String pass, String authType, String lastToken) throws Exception;
}
